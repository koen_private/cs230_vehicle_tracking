"""py-motmetrics - metrics for multiple object tracker (MOT) benchmarking.

Christoph Heindl, 2017
https://github.com/cheind/py-motmetrics
"""

import glob
import os
import logging
import motmetrics as mm
import pandas as pd
from collections import OrderedDict
from pathlib import Path
from pytorch_models.home_settings import home, repo_home
from pytorch_models.ssd.map.map_calc import map_calc
import datetime

def compare_dataframes(gts, ts):
    accs = []
    names = []
    for k, tsacc in ts.items():
        if k in gts:
            logging.info('Comparing {}...'.format(k))
            accs.append(mm.utils.compare_to_groundtruth(gts[k], tsacc, 'iou', distth=0.5))
            names.append(k)
        else:
            logging.warning('No ground truth for {}, skipping.'.format(k))

    return accs, names


def run_metrics(model_in='ssd', output_str='224_1e-4_20', target_size=224, postfix='', gtfiles_source = 'aicc_test'):
    solver = 'lapsolver'
    data_format = 'mot16'

    # loglevel = getattr(logging, '0', None)
    # if not isinstance(loglevel, int):
    #     raise ValueError('Invalid log level: {} '.format('info'))
    # logging.basicConfig(level=loglevel, format='%(asctime)s %(levelname)s - %(message)s', datefmt='%I:%M:%S')

    # if solver:
    #     mm.lap.default_solver = 'lapsolver'

    # tsfiles = [f for f in glob.glob(os.path.join(args.tests, '*.txt')) if not os.path.basename(f).startswith('eval')]
    gtfiles = [repo_home + 'datasets/output/' + model_in + '/gt_labels/aicc_test'+ postfix+'.txt']
    tsfiles = [repo_home + 'datasets/output/' + model_in + '/ts_results/predict_'+ output_str+'.txt']

    print('Found {} groundtruths and {} test files.'.format(len(gtfiles), len(tsfiles)))
    print('Available LAP solvers {}'.format(mm.lap.available_solvers))
    print('Default LAP solver \'{}\''.format(mm.lap.default_solver))
    print('Loading files.')

    # ts = OrderedDict([(os.path.splitext(Path(f).parts[-1])[0], mm.io.loadtxt(f, fmt=data_format)) for f in tsfiles])
    gt = OrderedDict([(Path(f).parts[-3], mm.io.loadtxt(f, fmt=data_format, min_confidence=0.5)) for f in gtfiles])
    # gt = OrderedDict(mm.io.loadtxt(gtfiles, fmt=data_format, min_confidence=0.5))
    # ts = OrderedDict(mm.io.loadtxt(tsfiles, fmt=data_format))
    ts = OrderedDict([(Path(f).parts[-3], mm.io.loadtxt(f, fmt=data_format)) for f in tsfiles])

    mh = mm.metrics.create()
    accs, names = compare_dataframes(gt, ts)

    logging.info('Running metrics')

    summary = mh.compute_many(accs, names=names, metrics=mm.metrics.motchallenge_metrics, generate_overall=True)
    result = mm.io.render_summary(summary, formatters=mh.formatters, namemap=mm.io.motchallenge_metric_names)
    map_output = map_calc(target_size=target_size, log_string=output_str, postfix=postfix)
    print(result)
    today = datetime.date.today().strftime("%Y_%m_%d")
    output_file_name = repo_home + model_in + '_eval_log_' + output_str +'_' + today + '.txt'
    text_file = open(output_file_name, "w")
    text_file.write(result)
    text_file.write(map_output)
    text_file.close()
    output_file_name_root = model_in + '_eval_log_' + output_str +'_' + today + '.txt'
    text_file = open(output_file_name_root, "w")
    text_file.write(result)
    text_file.write(map_output)
    text_file.close()

    logging.info('Completed')


if __name__ == '__main__':

    run_metrics(model_in='ssd', output_str='608_0.0001_60_2', target_size=224, postfix='', gtfiles_source = 'aicc_train')
# predict_608_0.0001_60_2.txt