import torch
import numpy as np
from pytorch_models.ssd import ssd_model_utils
from pytorch_models.ssd.ssd_model_setup import device
from torch.utils.data import Dataset, DataLoader

class PascalData(Dataset):
    # def __init__(self, data_list, ground_truths, classes, target_size=target_size, path=img_path):
    def __init__(self, data_list, ground_truths, classes, target_size, path, path_2):
        self.target_size = target_size
        self.path = path
        self.path_2 = path_2


        self.file_list = [i.filename for i in data_list]
        self.source_list = [i.source for i in data_list]
        self.ground_truths = ground_truths
        self.classes = classes

        self.mean = np.array([0.485, 0.456, 0.406]).reshape((3, 1, 1))
        self.std = np.array([0.229, 0.224, 0.225]).reshape((3, 1, 1))

    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, idx):
        if self.source_list[idx] == 'stanford':
            img_str = self.path_2 + self.file_list[idx]
        else:
            img_str = self.path + self.file_list[idx]

        img = ssd_model_utils.read_img(img_str, self.target_size)
        img = img / 255.0
        img = img.transpose((2, 0, 1))
        img = (img - self.mean) / self.std
        img = torch.from_numpy(img).float().to(device)

        return (img, self.ground_truths[idx], self.classes[idx])


class AI_Data(Dataset):
    # def __init__(self, data_list, ground_truths, classes, target_size=target_size, path=img_path):
    def __init__(self, data_list, ground_truths, classes, target_size, path, path_2):
        self.target_size = target_size
        self.path = path
        self.path_2 = path_2

        self.file_list = [i.filename for i in data_list]
        self.source_list = [i.source for i in data_list]
        self.ground_truths = ground_truths
        self.classes = classes

        self.mean = np.array([0.485, 0.456, 0.406]).reshape((3, 1, 1))
        self.std = np.array([0.229, 0.224, 0.225]).reshape((3, 1, 1))

    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, idx):
        if self.source_list[idx] == 'stanford':
            img_str = self.path_2 + self.file_list[idx]
        else:
            img_str = self.path + self.file_list[idx]

        img = ssd_model_utils.read_img(img_str, self.target_size)
        img = img / 255.0
        img = img.transpose((2, 0, 1))
        img = (img - self.mean) / self.std
        img = torch.from_numpy(img).float().to(device)

        return (img, self.ground_truths[idx], self.classes[idx])


def collate_fn(batch):
    """
    Writing custom collector function since the Dataset class returns both tensors and lists.
    """

    x = [b[0] for b in batch]
    x = torch.stack(x, dim=0)
    gt = [b[1] for b in batch]
    c = [b[2] for b in batch]
    return (x, gt, c)