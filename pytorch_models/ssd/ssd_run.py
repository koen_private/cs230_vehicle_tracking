#!/usr/bin/env python3
# Python packages
import numpy as np
import matplotlib.pyplot as plt
import torch

from torch.utils.data import Dataset, DataLoader
import torch.optim as optim
import os
import pandas as pd
import warnings
import datetime
warnings.filterwarnings("ignore", category=RuntimeWarning)

# source scripts
from pytorch_models.home_settings import repo_home
from pytorch_models.ssd.ssd_model_setup import IMG_PATH, IMG_PATH_AAIC, IMG_PATH_nexet, TRAIN_JSON, device, model_path, IMG_OUT, labels_AAIC_test, labels_AAIC_train, labels_nexet, labels_nexet_meta
from pytorch_models.ssd import ssd_model_utils
from pytorch_models.ssd.ssd_model import Model
from pytorch_models.ssd.ssd_model_utils_evaluation import non_max_suppression, PredBoundingBox, MAP
from pytorch_models.ssd.ssd_model_data import PascalData, collate_fn
from pytorch_models.ssd.ssd_model_functions_losses import SSDLoss, create_anchors
from pytorch_models.model_evaluate import run_metrics

device = device
print(device)
"""
Implemenation from 
https://cedrickchee.gitbook.io/knowledge/courses/fast.ai/deep-learning-part-2-cutting-edge-deep-learning-for-coders/2018-edition/lesson-9-multi-object-detection#multi-label-classification
and
https://github.com/parlstrand/ml_playground/blob/master/computer_vision/object_detection/ssd.ipynb
Predictions from three layers are used for the final set of predictions
"""

seed = 42

# params
# target_size = 224
batch_size = 32
wd = 0.0005
background_threshold = 0.5

image_source_path = IMG_PATH_nexet
image_source_path_2 = IMG_PATH_AAIC


def load_data(show_example=True, load_small=False, target_size=224):
    # id_cat, train_list = ssd_model_utils.load_pascal(TRAIN_JSON)
    id_cat, train_list = ssd_model_utils.load_stanford(labels_AAIC_train)
    # id_cat_2, train_list_2 = ssd_model_utils.load_stanford(labels_AAIC_train)
    # id_cat, train_list = ssd_model_utils.load_nexet(labels_nexet, labels_nexet_meta, filter='Day')
    # print(len(train_list), len(train_list_2))

    # id_cat.append(id_cat_2)
    try:
        train_list.extend(train_list_2)
    except:
        pass
    print(len(train_list))

    train_list = ssd_model_utils.rescale_bounding_boxes(train_list, target_size)
    train_list = ssd_model_utils.convert_to_center(train_list)
    # train_list_2 = ssd_model_utils.rescale_bounding_boxes(train_list_2, target_size)
    # train_list_2 = ssd_model_utils.convert_to_center(train_list_2)

    if load_small:
        # make sure that self labeled images are added
        train_list = train_list[:10]
        train_list.extend(train_list[-10:])

    print(len(train_list))

    if show_example:
        #todo make versatile for two image paths
        image_name = train_list[0]
        source = train_list[6]
        print(train_list[0])
        print(train_list[6])
        if source == 'stanford':
            img_str = image_source_path_2 + image_name.filename
        else:
            img_str = image_source_path + image_name.filename
        img = ssd_model_utils.read_img(img_str, target_size)

        img = ssd_model_utils.draw_boxes(img, image_name.bounding_boxes)
        img = ssd_model_utils.draw_text(img, image_name.classnames, image_name.bounding_boxes)
        plt.imshow(img)
        plt.show()

    ground_truths = []
    classes = []

    for item in train_list:
        gt = np.vstack(item.bounding_boxes)
        gt = torch.from_numpy(gt).float() / target_size
        ground_truths.append(gt[None, :, :].to(device))

        # Adding +1 since first element should be background
        c = torch.from_numpy(np.array(item.class_id))
        classes.append(c[None, :].to(device))

    train_dataset = PascalData(train_list, ground_truths, classes, target_size, image_source_path, image_source_path_2)

    return id_cat, train_dataset


def load_testdata(show_example=False, load_small=False, target_size=224):
    # id_cat, train_list = ssd_model_utils.load_pascal(TRAIN_JSON)
    id_cat, train_list = ssd_model_utils.load_stanford(labels_AAIC_test)
    # id_cat_2, train_list_2 = ssd_model_utils.load_stanford(labels_AAIC_train)
    # id_cat, train_list = ssd_model_utils.load_nexet(labels_nexet, labels_nexet_meta, filter='Day')
    # print(len(train_list), len(train_list_2))

    # id_cat.append(id_cat_2)
    # try:
    #     train_list.extend(train_list_2)
    # except:
    #     pass
    # print(len(train_list))

    train_list = ssd_model_utils.rescale_bounding_boxes(train_list, target_size)
    train_list = ssd_model_utils.convert_to_center(train_list)
    # train_list_2 = ssd_model_utils.rescale_bounding_boxes(train_list_2, target_size)
    # train_list_2 = ssd_model_utils.convert_to_center(train_list_2)

    if load_small:
        # make sure that self labeled images are added
        train_list = train_list[:100]
        train_list.extend(train_list[-100:])

    # print(len(train_list))

    if show_example:
        #todo make versatile for two image paths
        image_name = train_list[0]
        source = train_list[6]
        print(train_list[0])
        print(train_list[6])
        if source == 'stanford':
            img_str = image_source_path_2 + image_name.filename
        else:
            img_str = image_source_path + image_name.filename
        img = ssd_model_utils.read_img(img_str, target_size)

        img = ssd_model_utils.draw_boxes(img, image_name.bounding_boxes)
        img = ssd_model_utils.draw_text(img, image_name.classnames, image_name.bounding_boxes)
        plt.imshow(img)
        plt.show()

    ground_truths = []
    classes = []

    for item in train_list:
        gt = np.vstack(item.bounding_boxes)
        gt = torch.from_numpy(gt).float() / target_size
        ground_truths.append(gt[None, :, :].to(device))

        # Adding +1 since first element should be background
        c = torch.from_numpy(np.array(item.class_id))
        classes.append(c[None, :].to(device))

    train_dataset = PascalData(train_list, ground_truths, classes, target_size, image_source_path, image_source_path_2)

    return id_cat, train_dataset


def train(id_cat, train_dataset, anchors, num_boxes, target_size=224, learning_rate=1e-4, n_epochs=20, postfix=''):
    num_cat = len(id_cat)
    log_str_postfix = str(target_size) + '_' + str(learning_rate) + '_' + str(n_epochs) + postfix
    ' + log_str_postfix + '

    loss = SSDLoss(loc_factor=5.0, anchors=anchors, jaccard_overlap=0.4)

    torch.manual_seed(seed)
    model = Model(num_boxes=num_boxes, num_cat=num_cat).to(device)
    model.change_freezing(False)

    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=0, collate_fn=collate_fn)

    optimizer = optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=learning_rate, weight_decay=wd)

    torch.manual_seed(seed)
    for epoch in range(0, n_epochs + 1):
        model.train()
        train_loss = 0.0

        loc_loss = 0.0
        class_loss = 0.0
        i = 0

        for _, (x, bb_true, class_true) in enumerate(train_loader):
            i += 1
            model.zero_grad()
            class_hat, bb_hat = model(x)

            batch_loss, batch_loc, batch_class = loss(class_hat, bb_hat, class_true, bb_true, num_cat, device)
            batch_loss.backward()
            optimizer.step()

            class_loss += batch_class
            loc_loss += batch_loc
            train_loss += batch_loss

        print("number of training examples in current batch: {:.2f}".format(i))

        train_loss = (train_loss / len(train_loader)).detach().cpu().numpy()
        loc_loss = (loc_loss / len(train_loader)).detach().cpu().numpy()
        class_loss = (class_loss / len(train_loader)).detach().cpu().numpy()


        if epoch % 5 == 0:
            print("----- epoch {:.2f} ------".format(epoch))
            print("Train loss: {:.4f}".format(train_loss))
            print("Loc loss: {:.4f}".format(loc_loss))
            print("Class loss: {:.4f}".format(class_loss))

            today = datetime.date.today().strftime("%Y_%m_%d")
            output_file_name = repo_home + 'ssd' + 'run_log' + log_str_postfix + '_' + today + '.txt'
            text_file = open(output_file_name, "a+")
            text_file.write("----- epoch {:.2f} ------\n".format(epoch))
            text_file.write("Train loss: {:.4f}\n".format(train_loss))
            text_file.write("Loc loss: {:.4f}\n".format(loc_loss))
            text_file.write("Class loss: {:.4f}\n".format(class_loss))
            text_file.close()

            output_file_name_root = 'ssd' + 'root_run_log' + log_str_postfix + '_' + today + '.txt'
            text_file = open(output_file_name_root, "a+")
            text_file.write("----- epoch {:.2f} ------\n".format(epoch))
            text_file.write("Train loss: {:.4f}\n".format(train_loss))
            text_file.write("Loc loss: {:.4f}\n".format(loc_loss))
            text_file.write("Class loss: {:.4f}\n".format(class_loss))
            text_file.close()



    if not os.path.exists(model_path):
        os.makedirs(model_path)
    torch.save(model, os.path.join(model_path, 'trained_net_' + str(log_str_postfix)+'.pth'))
    torch.save(model.state_dict(), os.path.join(model_path, 'trained_net_state_' + str(log_str_postfix)+'.pth'))


def model_output(image_data, image_file, path, id_cat, anchors, num_boxes, show_image=True, save_image=True, target_size=224):
    # num_cat = len(id_cat)



    # model = Model(num_boxes=num_boxes, num_cat=num_cat).to(device)
    model = torch.load(os.path.join(model_path, 'trained_net.pth'))
    model.eval()

    # do the prediction
    class_hat, bb_hat = model(image_data.unsqueeze(0))

    # resize/replace the boxes to the size of the transformed input images (target_size)
    bb_hat = ssd_model_utils.invert_transformation(bb_hat.squeeze(0), anchors)
    bb_hat = bb_hat * target_size

    # pass the class predictions trough the output layer
    class_hat = class_hat.sigmoid().squeeze(0)

    # Filter low probabilities
    bb_hat = bb_hat[class_hat[:, 0] < background_threshold, :]
    bb_hat = bb_hat.detach().cpu().numpy()
    class_hat = class_hat[class_hat[:, 0] < background_threshold, :]

    class_preds = class_hat[:, 1:]
    prob, class_id = class_preds.max(1)

    prob = prob.detach().cpu().numpy()
    class_id = class_id.detach().cpu().numpy()

    output_bb = [PredBoundingBox(probability=prob[i],
                                 class_id=class_id[i],
                                 classname=id_cat[class_id[i]],
                                 bounding_box=[bb_hat[i, 0],
                                               bb_hat[i, 1],
                                               bb_hat[i, 2],
                                               bb_hat[i, 3]])
                 for i in range(len(prob))]

    output_bb = sorted(output_bb, key=lambda x: x.probability, reverse=True)

    # Apply non max suppression
    filtered_bb = non_max_suppression(output_bb)

    example_image = path + image_file

    img = ssd_model_utils.read_img(example_image, target_size)
    img = ssd_model_utils.draw_boxes(img, [bb.bounding_box for bb in filtered_bb])
    img = ssd_model_utils.draw_text(img, [bb.classname for bb in filtered_bb], [bb.bounding_box for bb in filtered_bb])

    predict_array = pd.DataFrame(np.array([bb.bounding_box for bb in filtered_bb]), columns=['box_x', 'box_y', 'box_width', 'box_height'])
    predict_array_labels = pd.Series(np.array([bb.classname for bb in filtered_bb]))
    predict_array_probs = pd.Series(np.array([bb.probability for bb in filtered_bb]))

    predict_array['image_filename'] = image_file
    predict_array['id'] = '-1'
    predict_array['col_u'] = '-1'
    predict_array['label_id'] = predict_array_labels
    predict_array['confidence'] = predict_array_probs

    keep_cols = ['image_filename', 'id', 'box_x', 'box_y', 'box_width', 'box_height', 'col_u', 'label_id', 'confidence']

    predict_array = predict_array[keep_cols]

    predict_array.to_csv(repo_home + 'datasets/output/ssd/test_results/predict.txt', index=False, header=False)

    if show_image:
        plt.imshow(img)
        plt.show()

    if save_image:
        plt.imshow(img)
        plt.savefig(os.path.join(IMG_OUT, image_file))

def model_eval_mAP(id_cat, train_dataset, anchors, num_boxes, jaccard_threshold=0.5, num_samples =50, target_size=224):

    num_cat = len(id_cat)

    model = Model(num_boxes=num_boxes, num_cat=num_cat).to(device)
    # model = torch.load(os.path.join(model_path, 'trained_net.pth'))
    # model.load_state_dict(torch.load(os.path.join(model_path, 'trained_net_state.pth')))
    model.load_state_dict(torch.load(os.path.join(model_path, 'trained_net_state_' + str(target_size) + '.pth')))  # ,map_location='cpu'
    model.eval()

    map_eval = MAP(model, jaccard_threshold, anchors)
    aps, mAP = map_eval(train_dataset, num_samples)
    print(mAP)

def model_eval(id_cat, test_dataset, anchors, num_boxes, save_image=True, target_size=224, learning_rate=1e-4, n_epochs=20, postfix=''):

    log_str_postfix = str(target_size) + '_' + str(learning_rate) + '_' + str(n_epochs) + postfix

    num_cat = len(id_cat)



    model = Model(num_boxes=num_boxes, num_cat=num_cat).to(device)
    # model = torch.load(os.path.join(model_path, 'trained_net_' + str(target_size)+'.pth'))
    # model.load_state_dict(torch.load(os.path.join(model_path, 'trained_net_state.pth')))trained_net_state_320.pth
    # model.load_state_dict(torch.load(os.path.join(model_path, 'trained_net_state_320_0.0001_20.pth'), map_location=device))
    model.load_state_dict(torch.load(os.path.join(model_path, 'trained_net_state_' + str(log_str_postfix)+'.pth'), map_location=device))

    model.eval()

    predict_array_all = pd.DataFrame()

    # image_data, image_file, path,
    for j in range(len(test_dataset)):
        (image_data, bb_true, class_true) = test_dataset[j]
        image_file = test_dataset.file_list[j]
        img_source = test_dataset.source_list[j]
        if img_source == 'stanford':
            path = image_source_path_2
        else:
            path = image_source_path
        class_hat, bb_hat = model(image_data.unsqueeze(0))

        # resize/replace the boxes to the size of the transformed input images (target_size)
        bb_hat = ssd_model_utils.invert_transformation(bb_hat.squeeze(0), anchors)
        bb_hat = bb_hat * target_size

        # pass the class predictions trough the output layer
        class_hat = class_hat.sigmoid().squeeze(0)

        # Filter low probabilities
        bb_hat = bb_hat[class_hat[:, 0] < background_threshold, :]
        bb_hat = bb_hat.detach().cpu().numpy()
        class_hat = class_hat[class_hat[:, 0] < background_threshold, :]

        class_preds = class_hat[:, 1:]
        prob, class_id = class_preds.max(1)

        prob = prob.detach().cpu().numpy()
        class_id = class_id.detach().cpu().numpy()

        output_bb = [PredBoundingBox(probability=prob[i],
                                     class_id=class_id[i],
                                     classname=id_cat[class_id[i]],
                                     bounding_box=[bb_hat[i, 0],
                                                   bb_hat[i, 1],
                                                   bb_hat[i, 2],
                                                   bb_hat[i, 3]])
                     for i in range(len(prob))]

        output_bb = sorted(output_bb, key=lambda x: x.probability, reverse=True)

        # Apply non max suppression
        filtered_bb = non_max_suppression(output_bb)

        example_image = path + image_file

        img = ssd_model_utils.read_img(example_image, target_size)
        img = ssd_model_utils.draw_boxes(img, [bb.bounding_box for bb in filtered_bb])
        img = ssd_model_utils.draw_text(img, [bb.classname for bb in filtered_bb], [bb.bounding_box for bb in filtered_bb])

        predict_array = pd.DataFrame(np.array([bb.bounding_box for bb in filtered_bb]), columns=['box_x', 'box_y', 'box_width', 'box_height'])
        predict_array_labels = pd.Series(np.array([bb.class_id for bb in filtered_bb]))
        predict_array_probs = pd.Series(np.array([bb.probability for bb in filtered_bb]))

        if img_source == 'stanford':
            predict_array['image_filename'] = int(image_file.split('.')[0])
        else:
            predict_array['image_filename'] = image_file
        predict_array['id'] = '-1'
        predict_array['col_1'] = '-1'
        predict_array['col_2'] = '-1'
        predict_array['label_id'] = predict_array_labels + 1
        predict_array['confidence'] = predict_array_probs

        keep_cols = ['image_filename', 'id', 'box_x', 'box_y', 'box_width', 'box_height', 'confidence', 'label_id', 'col_1', 'col_2']

        predict_array = predict_array[keep_cols]
        predict_array_all = predict_array_all.append(predict_array)

        if save_image:
            plt.imshow(img)
            plt.savefig(os.path.join(IMG_OUT, image_file))

    predict_array_all.sort_values('image_filename', inplace=True)
    predict_array_all.to_csv(repo_home + 'datasets/output/ssd/ts_results/predict_' + log_str_postfix + '.txt', index=False, header=False)
    run_metrics(model_in='ssd', output_str=log_str_postfix, target_size=target_size, postfix=postfix)

def run_main(mode='train', load_small=False, dataset='train', target_size=224, save_image=False, learning_rate=1e-4, n_epochs=20, postfix=''):
    # mode can be train, eval or example_output

    if dataset == 'train':
        id_cat, train_dataset = load_data(show_example=False, load_small=load_small, target_size=target_size)
        anchors, num_boxes = create_anchors()
        anchors = anchors.to(device)
        if mode == 'train':
            train(id_cat, train_dataset, anchors, num_boxes, target_size=target_size, learning_rate=learning_rate, n_epochs=n_epochs, postfix=postfix)
        if mode == 'eval':
            model_eval(id_cat, train_dataset, anchors, num_boxes, save_image=save_image, target_size=target_size, learning_rate=learning_rate, n_epochs=n_epochs, postfix=postfix)
        if mode == 'example_output':
            i = 5
            (x, bb_true, class_true) = train_dataset[i]
            img_file = train_dataset.file_list[i]
            img_source = train_dataset.source_list[i]
            if img_source == 'stanford':
                model_output(x, img_file, image_source_path_2, anchors, num_boxes, show_image=True, save_image=save_image, target_size=target_size)
            else:
                model_output(x, img_file, image_source_path, id_cat, anchors, num_boxes, show_image=True, save_image=save_image, target_size=target_size)

    elif dataset == 'test':
        id_cat, test_dataset = load_testdata(show_example=False, load_small=load_small, target_size=target_size)
        anchors, num_boxes = create_anchors()
        anchors = anchors.to(device)
        if mode == 'eval_map':
            model_eval_mAP(id_cat, test_dataset, anchors, num_boxes, target_size=target_size)
        if mode == 'eval':
            model_eval(id_cat, test_dataset, anchors, num_boxes, save_image=save_image, target_size=target_size, learning_rate=learning_rate, n_epochs=n_epochs, postfix=postfix)
        if mode == 'example_output':
            i = 5
            (x, bb_true, class_true) = test_dataset[i]
            img_file = test_dataset.file_list[i]
            img_source = test_dataset.source_list[i]
            if img_source == 'stanford':
                model_output(x, img_file, image_source_path_2, id_cat, anchors, num_boxes, show_image=True, save_image=save_image, target_size=target_size)
            else:
                model_output(x, img_file, image_source_path, id_cat, anchors, num_boxes, show_image=True, save_image=save_image, target_size=target_size)
    else:
        print("unknown testset")


if __name__ == "__main__":
    # print(sys.argv[1])
    run_main(mode='eval', load_small=False, dataset='test', target_size=608, save_image=True, learning_rate=1e-4, n_epochs=80, postfix='_2')
    # run_main(mode='eval', load_small=False, dataset='train', target_size=608, save_image=False, learning_rate=1e-4, n_epochs=60, postfix='_2')

    # run_main(mode='eval', load_small=False, dataset='test', target_size=224, save_image=True, learning_rate=1e-4, n_epochs=20, postfix='_2')
    # run_main(mode='train', load_small=False, dataset='train', target_size=320, save_image=True, learning_rate=1e-4, n_epochs=2)
    # run_main(mode='train', load_small=True, dataset='train')
    # run_metrics(model_in='ssd')

