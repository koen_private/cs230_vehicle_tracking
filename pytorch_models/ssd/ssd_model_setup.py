import torch
import os

from pytorch_models.home_settings import home, repo_home

"""
Implemenation from 
https://cedrickchee.gitbook.io/knowledge/courses/fast.ai/deep-learning-part-2-cutting-edge-deep-learning-for-coders/2018-edition/lesson-9-multi-object-detection#multi-label-classification
and
https://github.com/parlstrand/ml_playground/blob/master/computer_vision/object_detection/ssd.ipynb
"""

# home = '/home/koen/Documents/CS230/'
# repo_home = '/media/veracrypt1/Repository/cs230_vehicle_tracking/'

PATH = home + 'pascal_data'
TRAIN_JSON = os.path.join(PATH, 'pascal_train2012.json') #json.load(os.path.join(PATH, 'pascal_train2012.json').open())
IMG_PATH = os.path.join(PATH, 'VOCdevkit/VOC2012/JPEGImages/')
IMG_OUT = os.path.join(repo_home, 'datasets/output/ssd/images')
IMG_PATH_AAIC = os.path.join(home, 'Data/Track1/Loc1_1/img1_copy/')
IMG_PATH_nexet = os.path.join(home, 'Data/nexar/nexet_2017_train/nexet_2017_1/')

model_path = repo_home + 'pytorch_models/ssd/trained_model'

labels_AAIC_test = repo_home + 'datasets/train/S01/c001/gt/datasets_train_S01_c001_self_labeled_test_2.txt'
labels_AAIC_train = repo_home + 'datasets/train/S01/c001/gt/datasets_train_S01_c001_self_labeled_train_2.txt'

# use smaller set of nexet labels to not overpower the self labeled data
labels_nexet = os.path.join(repo_home, 'datasets/train/nexar/train_boxes_train.csv')
# labels_nexet = os.path.join(repo_home, 'datasets/train/nexar/train_boxes.csv')
labels_nexet_meta = os.path.join(repo_home, 'datasets/train/nexar/train.csv')

# IMAGES, ANNOTATIONS, CATEGORIES = ['images', 'annotations', 'categories']
# FILE_NAME, ID, IMG_ID, CAT_ID, BBOX = 'file_name', 'id', 'image_id', 'category_id', 'bbox'
#
# cats = dict((o[ID], o['name']) for o in trn_j[CATEGORIES])
# trn_fns = dict((o[ID], o[FILE_NAME]) for o in trn_j[IMAGES])
# trn_ids = [o[ID] for o in trn_j[IMAGES]]


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
