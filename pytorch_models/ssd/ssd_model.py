import torch
import torch.nn as nn
from torchvision import models
from pytorch_models.ssd.ssd_model_layers import StandardConv, OutputConv
import torch.nn.functional as F


class Model(nn.Module):
    def __init__(self, num_boxes, num_cat):
        super().__init__()

        pretrained_model = list(models.vgg16(pretrained='imagenet').children())[:-1]
        # pretrained_model = list(models.vgg16(pretrained=True).children())[:-1]
        self.pretrained_model = nn.Sequential(*pretrained_model)

        self.std_conv_1 = StandardConv(512, 256, stride=1)
        self.std_conv_2 = StandardConv(256, 256)
        self.std_conv_3 = StandardConv(256, 256)
        self.std_conv_4 = StandardConv(256, 256)

        self.out_conv_1 = OutputConv(256, num_boxes, num_cat)
        self.out_conv_2 = OutputConv(256, num_boxes, num_cat)
        self.out_conv_3 = OutputConv(256, num_boxes, num_cat)

    def forward(self, x):
        x = self.pretrained_model(x)
        x = F.relu(x)
        x = self.std_conv_1(x)
        x = self.std_conv_2(x)
        output_class_1, output_bb_1 = self.out_conv_1(x)

        x = self.std_conv_3(x)
        output_class_2, output_bb_2 = self.out_conv_2(x)

        x = self.std_conv_4(x)
        output_class_3, output_bb_3 = self.out_conv_3(x)

        # Class, bounding box
        return [torch.cat([output_class_1, output_class_2, output_class_3], dim=1),
                torch.cat([output_bb_1, output_bb_2, output_bb_3], dim=1)
                ]

    def change_freezing(self, mode=False):
        for param in self.pretrained_model.parameters():
            param.requires_grad = mode