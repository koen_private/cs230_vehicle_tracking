from pytorch_models.ssd.ssd_model_utils_evaluation import jaccard
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


def find_overlap(bb_true_i, anchors, jaccard_overlap):
    jaccard_tensor = jaccard(anchors, bb_true_i)
    _, max_overlap = torch.max(jaccard_tensor, dim=0)

    overlap_list = []
    for i in range(len(bb_true_i)):
        threshold_overlap = (jaccard_tensor[:, i] > jaccard_overlap).nonzero()

        if len(threshold_overlap) > 0:
            threshold_overlap = threshold_overlap[:, 0]
            overlap = torch.cat([max_overlap[i].view(1), threshold_overlap])
            overlap = torch.unique(overlap)
        else:
            overlap = max_overlap[i].view(1)
        overlap_list.append(overlap)
    return overlap_list


def create_anchors():

    anchor_grid = [4, 2, 1]  # Number of grid-elements per dimension
    anchor_zooms = [0.7, 1.0, 1.3]  # How much bigger/smaller each default box will be (percentage)
    anchor_ratios = [(1.0, 1.0), (1.0, 0.5), (0.5, 1.0)]  # Ratio between (width, height)

    anchor_scales = [(anc * h, anc * w) for anc in anchor_zooms for (h, w) in anchor_ratios]
    anchor_offsets = [1 / (2 * o) for o in anchor_grid]
    num_boxes = len(anchor_scales)

    anchor_x = [np.repeat(np.linspace(ao, 1 - ao, ag), ag) for (ao, ag) in zip(anchor_offsets, anchor_grid)]
    anchor_x = np.concatenate(anchor_x)

    anchor_y = [np.tile(np.linspace(ao, 1 - ao, ag), ag) for ao, ag in zip(anchor_offsets, anchor_grid)]
    anchor_y = np.concatenate(anchor_y)

    anchor_centers = np.repeat(np.stack([anchor_x, anchor_y], axis=1), num_boxes, axis=0)

    anchor_sizes = [np.array([[w / ag, h / ag] for _ in range(ag ** 2) for w, h in anchor_scales])
                    for ag in anchor_grid]
    anchor_sizes = np.concatenate(anchor_sizes)

    anchors = np.concatenate([anchor_centers, anchor_sizes], axis=1)
    anchors = torch.from_numpy(anchors).float()

    return anchors, num_boxes

class FocalLoss(nn.Module):
    def __init__(self, alpha=0.25, gamma=2, device="cuda", eps=1e-10):
        super().__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.device = device
        self.eps = eps

    def forward(self, input, target):
        p = torch.sigmoid(input)
        pt = p * target.float() + (1.0 - p) * (1 - target).float()
        alpha_t = (1.0 - self.alpha) * target.float() + self.alpha * (1 - target).float()
        loss = - 1.0 * torch.pow((1 - pt), self.gamma) * torch.log(pt + self.eps)
        return loss.sum()



class SSDLoss(nn.Module):
    def __init__(self, loc_factor, anchors, jaccard_overlap, **kwargs):
        super().__init__()
        self.fl = FocalLoss(**kwargs)
        self.loc_factor = loc_factor
        self.jaccard_overlap = jaccard_overlap
        self.anchors = anchors

    @staticmethod
    def one_hot_encoding(labels, num_classes):
        return torch.eye(num_classes)[labels]

    @staticmethod
    def loc_transformation(x, anchors, overlap_indicies):
        # Doing location transformations according to SSD paper
        return torch.cat([(x[:, 0:1] - anchors[overlap_indicies, 0:1]) / anchors[overlap_indicies, 2:3],
                          (x[:, 1:2] - anchors[overlap_indicies, 1:2]) / anchors[overlap_indicies, 3:4],
                          torch.log((x[:, 2:3] / anchors[overlap_indicies, 2:3])),
                          torch.log((x[:, 3:4] / anchors[overlap_indicies, 3:4]))
                         ], dim=1)

    def forward(self, class_hat, bb_hat, class_true, bb_true, num_cat, device):
        loc_loss = 0.0
        class_loss = 0.0

        for i in range(len(class_true)):  # Batch level
            class_hat_i = class_hat[i, :, :]
            bb_true_i = bb_true[i]
            class_true_i = class_true[i]
            class_target = torch.zeros(class_hat_i.shape[0]).long().to(device)

            overlap_list = find_overlap(bb_true_i.squeeze(0), self.anchors, self.jaccard_overlap)

            temp_loc_loss = 0.0
            for j in range(len(overlap_list)):  # BB level
                overlap = overlap_list[j]
                class_target[overlap] = class_true_i[0, j]

                input_ = bb_hat[i, overlap, :]
                target_ = SSDLoss.loc_transformation(bb_true_i[0, j, :].expand((len(overlap), 4)), self.anchors, overlap)

                temp_loc_loss += F.smooth_l1_loss(input=input_, target=target_, reduction="sum") / len(overlap)
            loc_loss += temp_loc_loss / class_true_i.shape[1]

            class_target = SSDLoss.one_hot_encoding(class_target, num_cat + 1).float().to(device)
            class_loss += self.fl(class_hat_i, class_target) / class_true_i.shape[1]

        loc_loss = loc_loss / len(class_true)
        class_loss = class_loss / len(class_true)
        loss = class_loss + loc_loss * self.loc_factor

        return loss, loc_loss, class_loss

