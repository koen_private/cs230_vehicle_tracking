import cv2
import numpy as np
import json
import pandas as pd
from collections import namedtuple
import torch
import csv
import os
from pytorch_models.ssd.ssd_model_setup import repo_home

"""
Use prescripted functions from
https://github.com/parlstrand/ml_playground/blob/master/computer_vision/object_detection/src/

"""

ImageEntry = namedtuple("ImageEntry", ["filename", "width", "height",
                                       "classnames", "class_id",
                                       "bounding_boxes", "source"
])


def read_img(img_str: str, target_size: int) -> np.ndarray:
    img = cv2.imread(img_str, cv2.IMREAD_UNCHANGED)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = cv2.resize(img, (target_size, target_size))
    return img


def draw_boxes(img: str, boxes: list) -> np.ndarray:
    for box in boxes:
        cv2.rectangle(img, (int(box[0] - box[2]/2), int(box[1] - box[3]/2)),
                      (int(box[0] + box[2]/2), int(box[1] + box[3]/2)),
                      (0, 0, 255), 2)

    return img


def draw_grid(img: str, pixel_step: int) -> np.ndarray:
    x = pixel_step
    y = pixel_step

    while x < img.shape[1]:
        cv2.line(img, (x, 0), (x, img.shape[0]), color=(255, 255, 255))
        x += pixel_step

    while y < img.shape[0]:
        cv2.line(img, (0, y), (img.shape[1], y), color=(255, 255, 255))
        y += pixel_step

    return img


def draw_text(img: str, texts: list, locations: list) -> np.ndarray:
    for text, loc in zip(texts, locations):
        cv2.putText(img, text, (int(loc[0]), int(loc[1])), cv2.FONT_HERSHEY_COMPLEX,
                    0.5, (255, 0, 0), 1)
    return img


def load_pascal(json_path: str) -> (dict, tuple):
    """
    Loading pascal bounding boxes and converting them to named tuples.
    """

    json_data = json.load(open(json_path))

    cats = json_data["categories"]
    id_cat = []

    for c in cats:
        id_cat.append([c["id"], c["name"]])

    df_cats = pd.DataFrame(id_cat, columns=["category_id", "name"])

    id_cat = {value-1: key for (value, key) in id_cat}

    df_filename = pd.DataFrame(json_data["images"])
    df_filename.columns = ["file_name", "height", "image_id", "width"]

    df_bbox = pd.DataFrame(json_data["annotations"])

    df = df_filename.merge(df_bbox, on="image_id")
    df = df[df["ignore"] == 0]
    df = df.drop(["area", "ignore", "iscrowd", "segmentation", "image_id"], axis=1)
    df = df.merge(df_cats, on="category_id")

    grouped_data = []

    grouped = df.groupby("file_name")
    for name, group in grouped:
        val = ImageEntry(filename=name, width=group["width"].values[0], height=group["height"].values[0],
                         classnames=list(group["name"].values), class_id=list(group["category_id"].values - 1),
                         bounding_boxes=list(group["bbox"].values), source="pascal")
        grouped_data.append(val)
    return id_cat, grouped_data


def load_stanford(txt_file: str) -> (dict, tuple):
    """
    Loading files and bounding boxes and converting them to named tuples.
    """

    # label_cats = pd.read_csv(os.path.join(home, 'vehicle_tracking/datasets/labels.txt'), names=["id", "label"], header=None)
    label_cats = pd.read_csv(os.path.join(repo_home, 'datasets/train/nexar/labels.csv'), names=["id", "label"], header=None)
    label_cats = label_cats.set_index(["id"])
    label_cats = label_cats["label"]
    id_cat_map = label_cats.to_list()

    id_cat = []
    for i, c in label_cats.iteritems():
        id_cat.append([i, c])

    id_cat = {value-1: key for (value, key) in id_cat}


    labels = pd.read_csv(txt_file, names=['image_number', 'col1', 'box_x', 'box_y', 'box_width', 'box_height', 'prob', 'col2', 'col3', 'col4', 'category_id'], header=None, dtype={'image_number': str})
    labels.dropna(inplace=True)


    labels['file_name'] = labels['image_number'].str.zfill(6) + '.jpg'
    labels['name'] = labels['file_name']
    labels['height'] = 1080
    labels['width'] = 1920
    labels = labels.round({'box_x': 0, 'box_y': 0, 'box_width': 0, 'box_height': 0, 'category_id': 0})
    labels[['box_x', 'box_y', 'box_width', 'box_height']] = labels[['box_x', 'box_y', 'box_width', 'box_height']].applymap(np.int64)
    # labels[['box_x', 'box_y', 'box_width', 'box_height', 'category_id']] = labels[['box_x', 'box_y', 'box_width', 'box_height', 'category_id']].applymap(np.int64)
    cols = ['box_x', 'box_y', 'box_width', 'box_height', 'category_id']
    labels[cols] = labels[cols].apply(pd.to_numeric, errors='coerce')

    labels['bbox'] = labels[['box_x', 'box_y', 'box_width', 'box_height']].values.tolist()#.mul(labels, axis=0)
    labels['label_name'] = labels['category_id'].replace(id_cat_map)

    grouped_data = []

    grouped = labels.groupby("file_name")
    for name, group in grouped:
        val = ImageEntry(filename=name, width=group["width"].values[0], height=group["height"].values[0],
                         classnames=list(group["label_name"].values), class_id=list(group["category_id"].values),
                         bounding_boxes=list(group["bbox"].values), source="stanford")
        grouped_data.append(val)

    return id_cat, grouped_data


def load_nexet(txt_file: str, txt_file_2: str, filter: str) -> (dict, tuple):
    """
    Loading files and bounding boxes and converting them to named tuples.
    """
    # / home / koen / Documents / CS230 / Data / nexar / labels.csv
    label_cats = pd.read_csv(os.path.join(repo_home, 'datasets/train/nexar/labels.csv'), names=["id", "label"], header=None)
    label_cats = label_cats.set_index(["id"])
    label_cats = label_cats["label"]
    id_cat_map = label_cats.to_list()

    id_cat = []
    for i, c in label_cats.iteritems():
        id_cat.append([i, c])

    id_cat = {value: key for (value, key) in id_cat}
    # id_cat[0] = 'background'
    cat_id = {value: key for key, value in id_cat_map.items()}
    # cat_id = label_cats_b.to_dict()

    labels_meta = pd.read_csv(txt_file_2)
    #, names=['image_number', 'col1', 'box_x', 'box_y', 'box_width', 'box_height', 'prob', 'col2', 'col3', 'col4', 'category_id'], header=None, dtype={'image_number': str})
    # labels_meta.dropna(inplace=True)
    labels_boxes = pd.read_csv(txt_file)
    labels = labels_boxes.merge(labels_meta, how='left', on='image_filename')

    labels.dropna(inplace=True)

    if filter is not 'none':
        labels = labels[labels['lighting'] == filter]

    labels.rename(columns={"image_filename": "file_name", "x0": "box_x", "y0": "box_y", "label": "label_name"}, inplace=True)
    # labels.rename(columns={"image_filename": "file_name", "x0": "box_x", "y1": "box_y", "label": "label_name"}, inplace=True)
    labels['box_width'] = labels['x1'] - labels['box_x']
    # labels['box_height'] = labels['box_y'] - labels['y0']
    labels['box_height'] = labels['y1'] - labels['box_y']

    # labels['file_name'] = labels['image_number'].str.zfill(6) + '.jpg'
    labels['name'] = labels['file_name']
    labels['height'] = 720
    labels['width'] = 1280
    labels = labels.round({'box_x': 0, 'box_y': 0, 'box_width': 0, 'box_height': 0})
    labels[['box_x', 'box_y', 'box_width', 'box_height']] = labels[['box_x', 'box_y', 'box_width', 'box_height']].applymap(np.int64)
    labels['bbox'] = labels[['box_x', 'box_y', 'box_width', 'box_height']].values.tolist()#.mul(labels, axis=0)
    labels['category_id'] = labels['label_name'].replace(cat_id)

    grouped_data = []

    grouped = labels.groupby("file_name")
    for name, group in grouped:
        val = ImageEntry(filename=name, width=group["width"].values[0], height=group["height"].values[0],
                         classnames=list(group["label_name"].values), class_id=list(group["category_id"].values),
                         bounding_boxes=list(group["bbox"].values), source="nexet")
        grouped_data.append(val)

    return id_cat, grouped_data


def rescale_bounding_boxes(data_list: list, target_size: int) -> list:
    """
    Rescaling the bounding boxes according to the new image size (target_size).
    """

    for i in range(len(data_list)):
        d = data_list[i]
        x_scale = target_size / d.width
        y_scale = target_size / d.height

        new_boxes = []
        for box in d.bounding_boxes:
            (x, y, d_x, d_y) = box

            x = int(round(x * x_scale))
            y = int(round(y * y_scale))
            d_x = int(round(d_x * x_scale))
            d_y = int(round(d_y * y_scale))

            new_boxes.append([x, y, d_x, d_y])

        data_list[i] = data_list[i]._replace(bounding_boxes=new_boxes)
    return data_list


def convert_to_center(data_list: list) -> list:
    """
    Converting [bx, by, w, h] to [cx, cy, w, h].
    """

    for i in range(len(data_list)):
        d = data_list[i]

        new_boxes = []
        for box in d.bounding_boxes:
            cx = box[0] + box[2]/2
            cy = box[1] + box[3]/2
            new_boxes.append([cx, cy, box[2], box[3]])
        data_list[i] = data_list[i]._replace(bounding_boxes=new_boxes)
    return data_list


def invert_transformation(bb_hat, anchors):
    """
    Invert the transform from "loc_transformation".
    """

    return torch.stack((anchors[:, 0] + bb_hat[:, 0] * anchors[:, 2],
                        anchors[:, 1] + bb_hat[:, 1] * anchors[:, 3],
                        anchors[:, 2] * torch.exp(bb_hat[:, 2]),
                        anchors[:, 3] * torch.exp(bb_hat[:, 3])
                        ), dim=1)