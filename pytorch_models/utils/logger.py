"""
creates a tf.log at log_dir
"""

import tensorflow as tf

class Logger():
    def __init__(self, log_dir):
        self.log_dir
        self.writer = tf.summary.FileWriter(log_dir)

    def summary(self, tag, value, step):
        """
        logs a scalar variable
        """
        summary = tf.Summary(value=[tf.Summary.Value(tag=tag, simple_value=value)])
        self.writer.add_summary(summary, step)

    def list_of_summary(self, tag_value_pairs, step):
        """
        list of scalar variables
        """
        summary = tf.Summary(value=[tf.Summary.Value(tag=tag, simple_value=value) for tag, value in tag_value_pairs])
        self.writer.add_summary(summary, step)