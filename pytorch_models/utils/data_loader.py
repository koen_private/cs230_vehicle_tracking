"""
functions to load data
"""

import os
import glob
import torch
import random
import numpy as np
import pandas as pd
import pytorch_models.utils.images as im
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms as transforms
from PIL import Image

def getClassNames(label_path):

    df = pd.read_csv(label_path)

    return df.class_names.values


def getDataLoader(dataset, batch_size, num_workers=2):

    dataloader = DataLoader(dataset, batch_size=batch_size,
                            shuffle=False, num_workers=num_workers,
                            collate_fn=dataset.collate_fn)

    return dataloader

class ImageFolder(Dataset):
    def __init__(self, folder_path, img_size=416):
        self.files = sorted(glob.glob("%s/*.*" %(folder_path)))
        self.img_size = img_size

    def __getitem(self, index):
        img_path = self.files[index % len(self.files)]
        # extract image as pytorch
        img = transforms.ToTensor()(Image.open(img_path))
        # pad to square resolution
        img, _ = im.pad_to_square(img, 0)
        #resize
        img = resize(img, self.img_size)

        return img_path, img

class ListDataset(Dataset):
    def __init__(self, list_path, img_size=416, augment=True,
                 multiscale=True, normalized_labels=True):
        with open(list_path, "r") as file:
            self.img_files = file.readlines()

        self.label_files = [path.replace("images", "labels").replace(".png", ".txt").replace(".jpg", ".txt")
                            for path in self.img_files]

        self.img_size = img_size
        self.max_objects = 100
        self.augment = augment
        self.multiscale = multiscale
        self.normalized_labels = normalized_labels
        self.min_size = self.img_size - 3 * 32
        self.max_size = self.img_size + 3 * 32
        self.batch_count = 0

    def __getitem__(self, index):

        img_path = self.img_files[index % len(self.img_files)].rstrip()

        # extract image as pytorch tensor
        img = transforms.ToTensor()(Image.open(img_path).convert("RGB"))

        # handle images with less than three channels
        if len(img.shape) != 3:
            img = img.unsqueeze(0)
            img = img.expand((3, img.shape[1:]))

        _, h, w = img.shape
        if self.normalized_labels:
            h_factor, w_factor = (h,w)
        else:
            h_factor, w_factor = (1, 1)

        # pad to square
        img, pad = im.pad_to_square(img, 0)
        _, padded_h, padded_w = img.shape

        # label
        label_path = self.label_files[index % len(self.img_files)].rstrip()

        targets = None
        if os.path.exists(label_path):
            boxes = torch.from_numpy(np.loadtxt(label_path).reshape(-1,5))
            # extract coordinates for unpadded + unscaled_image
            x1 = w_factor * (boxes[:, 1] - boxes[:, 3]/2)
            y1 = h_factor * (boxes[:, 2] - boxes[:, 4]/2)
            x2 = w_factor * (boxes[:, 1] - boxes[:, 3]/2)
            y2 = h_factor * (boxes[:, 2] - boxes[:, 4]/2)
            # adjust for added padding
            x1 += pad[0]
            y1 += pad[2]
            x2 += pad[1]
            y2 += pad[3]
            # returns (x, y, w, h)
            boxes[:, 1] = ((x1 + x2)/2) / padded_w
            boxes[:, 2] = ((y1 + y2)/2) / padded_h
            boxes[:, 3] *= w_factor / padded_w
            boxes[:, 4] *= h_factor / padded_h

            targets = torch.zeros((len(boxes), 6))
            targets[:, 1:] = boxes

        # apply augmentations
        if self.augment:
            if np.random.random() < 0.5:
                img, targets = im.horizontal_flip(img, targets)

        return img_path, img, targets

    def collate_fn(self, batch, n=10):
        paths, imgs, targets = list(zip(*batch))
        # remove empty placeholder
        targets = [boxes for boxes in targets if boxes is not None]
        # add sample index to targets
        for i, boxes in enumerate(targets):
            boxes[:, 0] = i
        targets = torch.cat(targets, 0)
        # select new image size every n batch
        if self.multiscale and self.batch_count % n == 0:
            self.img_size = random.choice(range(self.min_size,
                                                self.max_size + 1, 32))
        # resize images to input shape
        imgs = torch.stack([im.resize(img, self.img_size) for img in imgs])
        self.batch_count += 1

        return paths, imgs, targets
