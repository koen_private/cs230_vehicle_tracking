"""
utilities to transform bounding boxes
"""


def xywh2xyxy(x):
    """
    transfer from xywh to xyxy
    """

    y = x.new(x.shape)
    y[..., 0] = x[..., 0] - x[..., 2] / 2
    y[..., 1] = x[..., 1] - x[..., 3] / 2
    y[..., 2] = x[..., 0] + x[..., 2] / 2
    y[..., 3] = x[..., 1] + x[..., 3] / 2

    return y

def convert_to_center(data_list):
    """
    Converting [bx, by, w, h] to [cx, cy, w, h].
    """

    for i in range(len(data_list)):
        d = data_list[i]

        new_boxes = []
        for box in d.bounding_boxes:
            cx = box[0] + box[2]/2
            cy = box[1] + box[3]/2
            new_boxes.append([cx, cy, box[2], box[3]])
        data_list[i] = data_list[i]._replace(bounding_boxes=new_boxes)

    return data_list