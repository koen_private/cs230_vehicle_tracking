"""
evaluate function for validation
"""

import numpy as np
import pytorch_models.utils.metrics as mt
import pytorch_models.utils.box_utils as bu

import torch
from torch.autograd import Variable


def center_2_hw(box):
    """
    Converting (cx, cy, w, h) to (x1, y1, x2, y2)
    box: torch.Tensor
    """

    return torch.cat((box[:, 0, None] - box[:, 2, None]/2,
                      box[:, 1, None] - box[:, 3, None]/2,
                      box[:, 0, None] + box[:, 2, None]/2,
                      box[:, 1, None] + box[:, 3, None]/2
                      ), dim=1)

def intersect(box_a: torch.Tensor, box_b: torch.Tensor):
    """
    returns intersection value as a float
    box_a: torch.Tensor
    box_b: torch.Tensor
    """
    # Coverting (cx, cy, w, h) to (x1, y1, x2, y2) since its easier to extract min/max coordinates
    temp_box_a, temp_box_b = center_2_hw(box_a), center_2_hw(box_b)

    max_xy = torch.min(temp_box_a[:, None, 2:], temp_box_b[None, :, 2:])
    min_xy = torch.max(temp_box_a[:, None, :2], temp_box_b[None, :, :2])
    inter = torch.clamp((max_xy - min_xy), min=0)

    return inter[:, :, 0] * inter[:, :, 1]


def box_area(box: torch.Tensor):
    """
    returns box_area as float
    box: torch.Tensor
    """
    return box[:, 2] * box[:, 3]

def jaccard(box_a, box_b):
    """
    box_a: torch.Tensor
    box_b: torch.Tensor

    """
    intersection = intersect(box_a, box_b)
    union = box_area(box_a).unsqueeze(1) + box_area(box_b).unsqueeze(0) - intersection
    return intersection / union


def non_max_suppression(bounding_boxes, conf_thresh = 0.5):
    """
    bounding_boxes: list
    conf_threshold: float
    """
    filtered_bb = []

    while len(bounding_boxes) != 0:
        best_bb = bounding_boxes.pop(0)
        filtered_bb.append(best_bb)

        remove_items = []
        for bb in bounding_boxes:
            iou = jaccard(torch.tensor(best_bb.bounding_box).unsqueeze(0),
                          torch.tensor(bb.bounding_box).unsqueeze(0))

            if iou > conf_thresh:
                remove_items.append(bb)
        bounding_boxes = [bb for bb in bounding_boxes if bb not in remove_items]
    return filtered_bb

def evaluate(model, dataloader, iou_thresh, conf_thresh, img_size):

    model.eval()

    # check for cuda
    if torch.cuda.is_available():
        Tensor = torch.cuda.FloatTensor
    else:
        Tensor = torch.FloatTensor

    labels = []
    sample_metrics = []
    for batch_i, (_, imgs, targets) in enumerate(dataloader):

        # extract labels
        labels += targets[:, 1].tolist()
        #rescale target
        targets[:, 2:] = bu.xywh2xyxy(targets[:, 2:])
        targets[:, 2:] *= img_size

        imgs = Variable(imgs.type(Tensor), require_grad=False)

        with torch.no_grad():
            outputs = model(imgs)
            outputs = non_max_suppression(outputs, conf_thresh=conf_thresh)
        sample_metrics += mt.get_statistics(outputs, targets, iou_thresh=iou_thresh)

    true_pos, pred_scores, pred_labels = [np.concatenate(x, 0) for x in list(zip(*sample_metrics))]
    precision, recall, ap, f1, ap_class = mt.get_average_precision(true_pos, pred_scores, pred_labels, labels)

    return precision, recall, ap, f1, ap_class
