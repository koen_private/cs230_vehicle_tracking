"""
image utilities
"""

import numpy as np
import torch
import torch.nn.functional as F

def pad_to_square(img, pad_value):
    c, h, w = img.shape
    dim_diff = np.abs(h - w)
    # (upper / left) padding and (lower / right) padding
    pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
    # determine padding
    if h <= w:
        pad = (0, 0, pad1, pad2)
    else:
        pad = (pad1, pad2, 0, 0)
    # add padding
    img = F.pad(img, pad, "constant", value=pad_value)

    return img, pad

def resize(image, size):
    img = F.interpolate(image.unsqueeze(0), size=size, mode="nearest").squeeze(0)

    return img

def horizontal_flip(images, targets):
    images = torch.flip(images, [-1])
    targets[:, 2] = 1 - targets[:, 2]

    return images, targets