import torch
import os
from pprint import pprint

"""
Implemenation from 
https://cedrickchee.gitbook.io/knowledge/courses/fast.ai/deep-learning-part-2-cutting-edge-deep-learning-for-coders/2018-edition/lesson-9-multi-object-detection#multi-label-classification
and
https://github.com/parlstrand/ml_playground/blob/master/computer_vision/object_detection/ssd.ipynb
"""

home = '/home/koen/Documents/CS230/'
repo_home = '/media/veracrypt1/Repository/cs230_vehicle_tracking/'

PATH = home + 'pascal_data'
TRAIN_JSON = os.path.join(PATH, 'pascal_train2012.json') #json.load(os.path.join(PATH, 'pascal_train2012.json').open())
VOC_PATH = os.path.join(PATH, 'VOCdevkit/VOC2012/')
IMG_PATH = os.path.join(PATH, 'VOCdevkit/VOC2012/JPEGImages/')
IMG_OUT = os.path.join(PATH, 'VOCdevkit/VOC2012/Output/')
IMG_PATH_AAIC = os.path.join(home, 'Data/Track1/Loc1_1/img1_copy/')
IMG_PATH_nexet = os.path.join(home, 'Data/nexar/nexet_2017_train_1/nexet_2017_1/')

# model_path = repo_home + 'pytorch_models/ssd/trained_model'
model_path = repo_home + '/pytorch_models/faster_rcnn/trained_model/'
debug_file = model_path + '/debug'



labels_AAIC = home + 'vehicle_tracking/datasets/train/S01/c001/gt/gt_koen_Loc1_1.txt'
labels_nexet = os.path.join(home, 'Data/nexar/train_boxes.csv')
labels_nexet_meta = os.path.join(home, 'Data/nexar/train.csv')

# IMAGES, ANNOTATIONS, CATEGORIES = ['images', 'annotations', 'categories']
# FILE_NAME, ID, IMG_ID, CAT_ID, BBOX = 'file_name', 'id', 'image_id', 'category_id', 'bbox'
#
# cats = dict((o[ID], o['name']) for o in trn_j[CATEGORIES])
# trn_fns = dict((o[ID], o[FILE_NAME]) for o in trn_j[IMAGES])
# trn_ids = [o[ID] for o in trn_j[IMAGES]]


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

weight_decay = 0.0005
lr_decay = 0.1  # 1e-3 -> 1e-4
lr = 1e-3
epoch = 14
use_adam = False
rpn_sigma = 3.
roi_sigma = 1.
load_from_pretrain = False

# visualization
env = 'faster-rcnn'  # visdom env
port = 8097
plot_every = 40  # vis every N iter

use_adam = True  # Use Adam optimizer
use_chainer = False  # try match everything as chainer
use_drop = False  # use dropout in RoIHead
# debug

test_num = 10000
# model
load_path = None

# def _parse(self, kwargs):
#     state_dict = self._state_dict()
#     for k, v in kwargs.items():
#         if k not in state_dict:
#             raise ValueError('UnKnown Option: "--%s"' % k)
#         setattr(self, k, v)
#
#     print('======user config========')
#     pprint(self._state_dict())
#     print('==========end============')
#
#
# def _state_dict(self):
#     return {k: getattr(self, k) for k, _ in Config.__dict__.items() \
#             if not k.startswith('_')}
