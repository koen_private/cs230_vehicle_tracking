from pytorch_models.faster_rcnn.frcnn_layers import Vgg16
import torch.utils.model_zoo as model_zoo
import torch.nn as nn


def vgg_with_batch(nr_classes=1000, pretrained=False):

    if pretrained:
        init_weights = False
        vgg16 = Vgg16(nr_classes=nr_classes, init_weights=init_weights)
    else:
        vgg16 = Vgg16(nr_classes=nr_classes)

    if pretrained:
        # to train with costum classes; 1000 is normal mode, otherwise replace weights of last layer with random initialized weights
        if nr_classes == 1000:
            vgg16.load_state_dict(model_zoo.load_url('https://download.pytorch.org/models/vgg16_bn-6c64b313.pth'))
        else:
            state_dict = model_zoo.load_url('https://download.pytorch.org/models/vgg16_bn-6c64b313.pth')
            # model_dict = model.state_dict()
            # weights will be randomly initialized
            # state_dict = {k: v for k, v in state_dict.items() if k in model_dict}
            del state_dict["classifier.6.weight"]
            del state_dict["classifier.6.bias"]
            # model_dict.update(state_dict)
            vgg16.load_state_dict(state_dict, strict=False)

    # vgg16.classifier = nn.Sequential(*list(vgg16.classifier._modules.values())[:-1])
    RCNN_top = nn.Sequential(*list(vgg16.classifier._modules.values())[:-1])

    RCNN_base = nn.Sequential(*list(vgg16.features._modules.values())[:-1])

    # fix layers while training
    for layer in range(10):
        for p in RCNN_base[layer].parameters(): p.requires_grad = False

    # deliver a class score
    RCNN_cls_score = nn.Linear(4096, nr_classes)

    # deliver a box score
    RCNN_bbox_pred = nn.Linear(4096, 4 * nr_classes)
    return RCNN_top, RCNN_base

def _head_to_tail(self, pool5):

    pool5_flat = pool5.view(pool5.size(0), -1)
    fc7 = self.RCNN_top(pool5_flat)

    return fc7
