import torch.nn as nn
import torch.nn.functional as F


class Vgg16(nn.Module):

    # classes: int
    #, init_weights=True

    def __init__(self, nr_classes, init_weights=True, kernel=3, padding=1, stride=1, pool=2):

        super(Vgg16, self).__init__()
        self.classes = nr_classes
        self.features = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=64, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=64),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=64),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=pool, stride=pool, dilation=1),

            # X = F.relu(self.conv2d(in_channels=64, out_channels=128))
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=128),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=128, out_channels=128, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=128),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=pool, stride=pool, dilation=1),

            # X = F.relu(self.conv2d(in_channels=128, out_channels=256))
            # X = F.relu(self.conv2d(in_channels=256, out_channels=256))
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=256),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=256),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=256),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=pool, stride=pool,  dilation=(1, 1)),

            # X = F.relu(self.conv2d(in_channels=256, out_channels=512))
            # X = F.relu(self.conv2d(in_channels=512, out_channels=512))
            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=512),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=512),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=512),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=pool, stride=pool, dilation=1),

            # X = F.relu(self.conv2d(in_channels=512, out_channels=512))
            # X = F.relu(self.conv2d(in_channels=512, out_channels=512))
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=512),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=512),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=kernel, padding=padding, stride=stride),
            nn.BatchNorm2d(num_features=512),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=pool, stride=pool, dilation=1),
        )
        self.avgpool = nn.AdaptiveAvgPool2d((7, 7))

        self.classifier = nn.Sequential(
            nn.Linear(in_features=25088, out_features=4096),
            nn.ReLU(inplace=True),
            nn.Dropout(0.5), #Dropout(0.5, training=train),
            nn.Linear(in_features=4096, out_features=4096),
            nn.ReLU(inplace=True),
            nn.Dropout(0.5),
            nn.Linear(in_features=4096, out_features=self.classes),
            # nn.ReLU(nn.Linear(in_features=4096, out_features=self.classes)),
        )

        if init_weights:
            self._initialize_weights()
        elif nr_classes != 1000:
            # e = len(self.modules())
            i = 0
            for m in self.classifier.modules():
                i += 1
                if isinstance(m, nn.Linear) and i == 8:
                    nn.init.normal_(m.weight, 0, 0.01)
                    nn.init.constant_(m.bias, 0)



    def forward(self, x):

        #  F.relu(self.conv(x, in_channels=3, out_channels=64))

        x = self.features(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)

        return x

    def predict(self, x):

        # X = F.softmax(self.forward(x)) # , train=False
        return x

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(
                    m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)


class RpnLayers(nn.Module):

    # classes: int
    #, init_weights=True

    def __init__(self, nr_classes, nr_anchors=9, init_weights=True, kernel_base=3, kernel_predict=1, padding_base=1, padding_predict=0, stride=1):

        super(RpnLayers, self).__init__()
        self.classes = nr_classes

        self.base_conv_layer = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=kernel_base, stride=stride, padding=padding_base)
        self.box_conv_layer = nn.Conv2d(in_channels=512, out_channels=nr_anchors * 4, kernel_size=kernel_predict, stride=stride, padding=padding_predict)
        self.clas_conv_layer = nn.Conv2d(in_channels=512, out_channels=nr_anchors * nr_classes, kernel_size=kernel_predict, stride=stride, padding=padding_predict)

        if init_weights:
            self._initialize_weights()

    def forward(self, x):

        # # X.view(-1, 25088)

        X = self.base_conv_layer(x)

        Anchor_loc = self.box_conv_layer(X)
        Clas_scores = self.clas_conv_layer(X)

        Clas_scores = Clas_scores.permute(0, 2, 3, 1).contiguous()
        Clas_scores = Clas_scores.view(1, -1, self.classes)
        Objectness_score = Clas_scores.view(1, 50, 50, 9, self.classes)[:, :, :, :, 1].contiguous().view(1, -1)

        Anchor_loc = Anchor_loc.permute(0, 2, 3, 1).contiguous().view(1, -1, 4)

        return Clas_scores, Objectness_score, Anchor_loc

    def predict(self, x):

        X = F.softmax(self.forward(x)) # , train=False
        return X

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.normal_(m.weight, 0, 0.01)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
