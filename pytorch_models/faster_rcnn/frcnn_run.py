import numpy as np
import matplotlib.pyplot as plt
import torch
from torchvision import models
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
import torch.optim as optim

from pytorch_models.frcnn_jwyang_implement.frcnn_model_setup import device
from pytorch_models.frcnn_jwyang_implement.frcnn_utils import create_anchors, create_targets, gen_proposals, proposal_targets
from pytorch_models.frcnn_jwyang_implement.rpn_model import rpn
from pytorch_models.frcnn_jwyang_implement.vgg_model import vgg_with_batch
from pytorch_models.frcnn_jwyang_implement.faster_rcnn_model import fast_rnn

device = device

def train(test_image, test_bbox, test_labels, img_size_in = 800, training=True):

    model = vgg_with_batch(nr_classes=1000, pretrained=True)

    base_feat = model.RCNN_base.init_modules()

    out_map = base_feat(test_image)
    print(out_map.size())

    anchors = create_anchors(img_size_in)
    anchor_locations, anchor_labels = create_targets(anchors)

    rpn_model = rpn(nr_classes=2, pretrained=True).to(device)

    class_scores, objectness_scores, anchor_locations = rpn_model(out_map)

    print(class_scores.shape, objectness_scores.shape, anchor_locations.shape)

    region_of_interest = gen_proposals(anchors, objectness_scores, anchor_locations, img_size_in, training=True)

    print(region_of_interest.shape)

    gt_roi_labels, gt_roi_locs, sample_roi = proposal_targets(roi=region_of_interest, bbox=test_bbox, labels=test_labels)
    print(gt_roi_labels.shape, gt_roi_locs.shape, sample_roi.shape)

    fast_rnn(sample_rois=sample_roi, x=out_map)


if __name__== "__main__":
    test_image = torch.zeros((1, 3, 800, 800)).float()
    test_bbox = np.asarray([[20, 30, 400, 500], [300, 400, 500, 600]], dtype=np.float32)
    test_labels = np.asarray([6, 8], dtype=np.int8)

    train(test_image, test_bbox, test_labels)

    # print(test_image)
