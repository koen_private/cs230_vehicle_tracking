from __future__ import  absolute_import
# though cupy is not used but without this line, it raise errors...
# import cupy as cp
import os

# import ipdb
import matplotlib
from tqdm import tqdm

# from utils.config import opt
from pytorch_models.faster_rcnn.utils.dataset import Dataset, TestDataset
from pytorch_models.faster_rcnn.utils.dataset import inverse_normalize

from pytorch_models.faster_rcnn.faster_rcnn_vgg import FasterRCNNVGG16
from torch.utils import data as data_
from pytorch_models.faster_rcnn.trainer import FasterRCNNTrainer
from pytorch_models.faster_rcnn.utils import array_tool as at
from pytorch_models.faster_rcnn.utils.vis_tool import visdom_bbox
from pytorch_models.faster_rcnn.utils.eval_tool import eval_detection_voc
from pytorch_models.faster_rcnn.frcnn_model_setup import load_from_pretrain, model_path, VOC_PATH, debug_file
from pytorch_models.faster_rcnn.frcnn_model_setup import lr, epoch, plot_every, test_num, lr_decay

from pytorch_models.faster_rcnn.frcnn_model_setup import device

device = device


# fix for ulimit
# https://github.com/pytorch/pytorch/issues/973#issuecomment-346405667
import resource

rlimit = resource.getrlimit(resource.RLIMIT_NOFILE)
# resource.setrlimit(resource.RLIMIT_NOFILE, (20480, rlimit[1]))
resource.setrlimit(resource.RLIMIT_NOFILE, (4096, rlimit[1]))


matplotlib.use('agg')


def eval(dataloader, faster_rcnn, test_num=10000):
    pred_bboxes, pred_labels, pred_scores = list(), list(), list()
    gt_bboxes, gt_labels, gt_difficults = list(), list(), list()
    for ii, (imgs, sizes, gt_bboxes_, gt_labels_, gt_difficults_) in tqdm(enumerate(dataloader)):
        sizes = [sizes[0][0].item(), sizes[1][0].item()]
        pred_bboxes_, pred_labels_, pred_scores_ = faster_rcnn.predict(imgs, [sizes])
        gt_bboxes += list(gt_bboxes_.numpy())
        gt_labels += list(gt_labels_.numpy())
        gt_difficults += list(gt_difficults_.numpy())
        pred_bboxes += pred_bboxes_
        pred_labels += pred_labels_
        pred_scores += pred_scores_
        if ii == test_num: break

    result = eval_detection_voc(
        pred_bboxes, pred_labels, pred_scores,
        gt_bboxes, gt_labels, gt_difficults,
        use_07_metric=True)
    return result


def train(**kwargs):
    # opt._parse(kwargs)

    dataset = Dataset(VOC_PATH)
    print('load data')
    dataloader = data_.DataLoader(dataset, \
                                  batch_size=1, \
                                  shuffle=True, \
                                  # pin_memory=True,
                                  num_workers=2)
    testset = TestDataset(VOC_PATH)
    test_dataloader = data_.DataLoader(testset,
                                       batch_size=1,
                                       num_workers=2,
                                       shuffle=False, \
                                       pin_memory=True
                                       )
    faster_rcnn = FasterRCNNVGG16()
    print('model construct completed')
    trainer = FasterRCNNTrainer(faster_rcnn).to(device)
    if load_from_pretrain:
        trainer.load(model_path)
        print('load pretrained model from %s' % model_path)
    trainer.vis.text(dataset.db.label_names, win='labels')
    best_map = 0
    for epochs in range(epoch):
        trainer.reset_meters()
        for ii, (img, bbox_, label_, scale) in tqdm(enumerate(dataloader)):
            scale = at.scalar(scale)
            img, bbox, label = img.to(device).float(), bbox_.to(device), label_.to(device)
            trainer.train_step(img, bbox, label, scale)

            if (ii + 1) % plot_every == 0:
                # if os.path.exists(debug_file):
                #     ipdb.set_trace()

                # plot loss
                trainer.vis.plot_many(trainer.get_meter_data())

                # plot groud truth bboxes
                ori_img_ = inverse_normalize(at.tonumpy(img[0]))
                gt_img = visdom_bbox(ori_img_,
                                     at.tonumpy(bbox_[0]),
                                     at.tonumpy(label_[0]))
                trainer.vis.img('gt_img', gt_img)

                # plot predicti bboxes
                _bboxes, _labels, _scores = trainer.faster_rcnn.predict([ori_img_], visualize=True)
                pred_img = visdom_bbox(ori_img_,
                                       at.tonumpy(_bboxes[0]),
                                       at.tonumpy(_labels[0]).reshape(-1),
                                       at.tonumpy(_scores[0]))
                trainer.vis.img('pred_img', pred_img)

                # rpn confusion matrix(meter)
                trainer.vis.text(str(trainer.rpn_cm.value().tolist()), win='rpn_cm')
                # roi confusion matrix
                trainer.vis.img('roi_cm', at.totensor(trainer.roi_cm.conf, False).float())
        eval_result = eval(test_dataloader, faster_rcnn, test_num=test_num)
        trainer.vis.plot('test_map', eval_result['map'])
        lr_ = trainer.faster_rcnn.optimizer.param_groups[0]['lr']
        log_info = 'lr:{}, map:{},loss:{}'.format(str(lr_),
                                                  str(eval_result['map']),
                                                  str(trainer.get_meter_data()))
        trainer.vis.log(log_info)

        if eval_result['map'] > best_map:
            best_map = eval_result['map']
            best_path = trainer.save(best_map=best_map)
        if epoch == 9:
            trainer.load(best_path)
            trainer.faster_rcnn.scale_lr(lr_decay)
            lr_ = lr_ * lr_decay

        if epoch == 13: 
            break


if __name__ == '__main__':
    # import fire

    # fire.Fire()
    train()
