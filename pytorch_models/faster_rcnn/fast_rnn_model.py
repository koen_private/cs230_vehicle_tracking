import numpy as np
import torch
import torch.nn as nn


def fast_rnn(sample_rois, x):

    rois = torch.from_numpy(sample_rois).float()
    roi_indices = 0 * np.ones((len(rois),), dtype=np.int32)
    roi_indices = torch.from_numpy(roi_indices).float()

    indices_and_rois = torch.cat([roi_indices[:, None], rois], dim=1)
    xy_indices_and_rois = indices_and_rois[:, [0, 2, 1, 4, 3]]
    indices_and_rois = xy_indices_and_rois.contiguous()

    size = (7, 7)
    adaptive_max_pool = nn.AdaptiveMaxPool2d(size[0], size[1])

    output = []
    rois = indices_and_rois.data.float()
    rois[:, 1:].mul_(1 / 16.0)  # Subsampling ratio
    rois = rois.long()
    num_rois = rois.size(0)
    for i in range(num_rois):
        roi = rois[i]
        im_idx = roi[0]
        im = x.narrow(0, im_idx, 1)[..., roi[2]:(roi[4] + 1), roi[1]:(roi[3] + 1)]
        output.append(adaptive_max_pool(im))

    output = torch.cat(output, 0)
    print(output.size())



    # Reshape the tensor so that we can pass it through the feed forward layer.
    k = output.view(output.size(0), -1)
    print(k.shape)
