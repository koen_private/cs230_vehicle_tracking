import numpy as np


def create_anchors(img_size_in):
    factor = img_size_in // 16

    ratio = [0.5, 1, 2]
    anchor_scales = [8, 16, 32]

    sub_sample = 16

    # anchor_base = np.zeros((len(ratio) * len(anchor_scales), 4), dtype=np.float32)
    #
    # center_x = sub_sample / 2.
    # center_y = sub_sample / 2.
    #
    # for i in range(len(ratio)):
    #     for j in range(len(anchor_scales)):
    #         h = sub_sample * anchor_scales[j] * np.sqrt(ratio[i])
    #         w = sub_sample * anchor_scales[j] * np.sqrt(1. / ratio[i])
    #
    #         index = i * len(anchor_scales) + j
    #
    #         anchor_base[index, 0] = center_y - h / 2.
    #         anchor_base[index, 1] = center_x - w / 2.
    #         anchor_base[index, 2] = center_y + h / 2.
    #         anchor_base[index, 3] = center_x + w / 2.

    center_x = np.arange(16, (factor + 1) * 16, 16)
    center_y = np.arange(16, (factor + 1) * 16, 16)

    ctr = np.zeros((len(center_y) * len(center_x), 2))

    index = 0
    for x in range(len(center_x)):
        for y in range(len(center_y)):
            ctr[index, 1] = center_x[x] - 8
            ctr[index, 0] = center_y[y] - 8
            index += 1

    anchors = np.zeros((factor * factor * 9, 4))

    index = 0
    for c in ctr:
        ctr_y, ctr_x = c
        for i in range(len(ratio)):
            for j in range(len(anchor_scales)):
                h = sub_sample * anchor_scales[j] * np.sqrt(ratio[i])
                w = sub_sample * anchor_scales[j] * np.sqrt(1. / ratio[i])

                anchors[index, 0] = ctr_y - h / 2.
                anchors[index, 1] = ctr_x - w / 2.
                anchors[index, 2] = ctr_y + h / 2.
                anchors[index, 3] = ctr_x + w / 2.
                index += 1


    return anchors

def create_targets(anchors, pos_iou_threshold=0.7, neg_iou_threshold=0.3, pos_ratio=0.5, n_sample=256):
    bbox = np.asarray([[20, 30, 400, 500], [300, 400, 500, 600]], dtype=np.float32)  # [y1, x1, y2, x2] format

    n_pos = pos_ratio * n_sample

    index_inside = np.where(
        (anchors[:, 0] >= 0) &
        (anchors[:, 1] >= 0) &
        (anchors[:, 2] <= 800) &
        (anchors[:, 3] <= 800)
    )[0]

    label = np.empty((len(index_inside),), dtype=np.int32)
    label.fill(-1)

    valid_anchor_boxes = anchors[index_inside]

    ious = calc_ious(valid_anchor_boxes, bbox)

    gt_argmax_ious = ious.argmax(axis=0)

    gt_max_ious = ious[gt_argmax_ious, np.arange(ious.shape[1])]

    argmax_ious = ious.argmax(axis=1)

    max_ious = ious[np.arange(len(index_inside)), argmax_ious]

    gt_argmax_ious = np.where(ious == gt_max_ious)[0]

    label[max_ious < neg_iou_threshold] = 0

    label[gt_argmax_ious] = 1

    label[max_ious >= pos_iou_threshold] = 1

    pos_index = np.where(label == 1)[0]

    if len(pos_index) > n_pos:
        disable_index = np.random.choice(pos_index, size=(len(pos_index) - n_pos), replace=False)
        label[disable_index] = -1

    n_neg = n_sample * np.sum(label == 1)
    neg_index = np.where(label == 0)[0]

    if len(neg_index) > n_neg:
        disable_index = np.random.choice(neg_index, size=(len(neg_index) - n_neg), replace=False)
        label[disable_index] = -1

    max_iou_bbox = bbox[argmax_ious]

    height = valid_anchor_boxes[:, 2] - valid_anchor_boxes[:, 0]
    width = valid_anchor_boxes[:, 3] - valid_anchor_boxes[:, 1]
    ctr_y = valid_anchor_boxes[:, 0] + 0.5 * height
    ctr_x = valid_anchor_boxes[:, 1] + 0.5 * width

    base_height = max_iou_bbox[:, 2] - max_iou_bbox[:, 0]
    base_width = max_iou_bbox[:, 3] - max_iou_bbox[:, 1]
    base_ctr_y = max_iou_bbox[:, 0] + 0.5 * base_height
    base_ctr_x = max_iou_bbox[:, 1] + 0.5 * base_width

    eps = np.finfo(height.dtype).eps
    height = np.maximum(height, eps)
    width = np.maximum(width, eps)

    dy = (base_ctr_y - ctr_y) / height
    dx = (base_ctr_x - ctr_x) / width
    dh = np.log(base_height / height)
    dw = np.log(base_width / width)

    anchor_locs = np.vstack((dy, dx, dh, dw)).transpose()

    anchor_labels = np.empty((len(anchors),), dtype=label.dtype)
    anchor_labels.fill(-1)
    anchor_labels[index_inside] = label

    anchor_locations = np.empty((len(anchors),) + anchors.shape[1:], dtype=anchor_locs.dtype)
    anchor_locations.fill(0)
    anchor_locations[index_inside, :] = anchor_locs

    return anchor_locations, anchor_labels


def calc_ious(valid_anchor_boxes, bbox):
    ious = np.empty((len(valid_anchor_boxes), 2), dtype=np.float32)
    ious.fill(0)

    for num1, i in enumerate(valid_anchor_boxes):
        ya1, xa1, ya2, xa2 = i
        anchor_area = (ya2 - ya1) * (xa2 - xa1)
        for num2, j in enumerate(bbox):
            yb1, xb1, yb2, xb2 = j
            box_area = (yb2 - yb1) * (xb2 - xb1)

            inter_x1 = max([xb1, xa1])
            inter_y1 = max([yb1, ya1])
            inter_x2 = min([xb2, xa2])
            inter_y2 = min([yb2, ya2])

            if (inter_x1 < inter_x2) and (inter_y1 < inter_y2):
                iter_area = (inter_y2 - inter_y1) * \
                            (inter_x2 - inter_x1)
                iou = iter_area / \
                      (anchor_area + box_area - iter_area)
            else:
                iou = 0.

            ious[num1, num2] = iou

    return ious

def gen_proposals(anchors, objectness_scores, anchor_locations, img_size_in, training=True, nms_thresh=0.7
                  , n_train_pre_nms=12000, n_train_post_nms=2000, n_test_pre_nms=2000, n_test_post_nms=300, min_size=16):

    # including non-max suppression

    anc_height = anchors[:, 2] - anchors[:, 0]
    anc_width = anchors[:, 3] - anchors[:, 1]
    anc_ctr_y = anchors[:, 0] + 0.5 * anc_height
    anc_ctr_x = anchors[:, 1] + 0.5 * anc_width

    anchor_locations_numpy = anchor_locations[0].data.numpy()
    objectness_scores_numpy = objectness_scores[0].data.numpy()

    dy = anchor_locations_numpy[:, 0::4]
    dx = anchor_locations_numpy[:, 1::4]
    dh = anchor_locations_numpy[:, 2::4]
    dw = anchor_locations_numpy[:, 3::4]

    ctr_y = dy * anc_height[:, np.newaxis] + anc_ctr_y[:, np.newaxis]
    ctr_x = dx * anc_width[:, np.newaxis] + anc_ctr_x[:, np.newaxis]
    h = np.exp(dh) * anc_height[:, np.newaxis]
    w = np.exp(dw) * anc_width[:, np.newaxis]

    roi = np.zeros(anchor_locations_numpy.shape, dtype=anchor_locations_numpy.dtype)
    roi[:, 0::4] = ctr_y - 0.5 * h
    roi[:, 1::4] = ctr_x - 0.5 * w
    roi[:, 2::4] = ctr_y + 0.5 * h
    roi[:, 3::4] = ctr_x + 0.5 * w

    img_size = (img_size_in, img_size_in)  # Image size
    roi[:, slice(0, 4, 2)] = np.clip(
        roi[:, slice(0, 4, 2)], 0, img_size[0])
    roi[:, slice(1, 4, 2)] = np.clip(
        roi[:, slice(1, 4, 2)], 0, img_size[1])

    # print(roi)

    hs = roi[:, 2] - roi[:, 0]
    ws = roi[:, 3] - roi[:, 1]
    keep = np.where((hs >= min_size) & (ws >= min_size))[0]
    roi = roi[keep, :]
    score = objectness_scores_numpy[keep]

    # note diverts from documentation

    order = score.ravel().argsort()[::-1]

    if training:
        order = order[:n_train_pre_nms]
    else:
        order = order[:n_test_pre_nms]

    roi = roi[order, :]

    # non max suppression

    y1 = roi[:, 0]
    x1 = roi[:, 1]
    y2 = roi[:, 2]
    x2 = roi[:, 3]

    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = order.argsort()[::-1]

    keep = []

    while order.size > 0:

        i = order[0]
        keep.append(i)

        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        overlap = (w * h) / (area[i] + area[order[1:]] - (w * h))

        reloop_indexes = np.where(overlap <= nms_thresh)[0]
        order = order[reloop_indexes + 1]

    if training:
        keep = keep[:n_train_post_nms]
    else:
        keep = keep[:n_test_post_nms]

    roi = roi[keep]  # the final region proposals

    return roi

def proposal_targets(roi, bbox, labels, sample=128, pos_ratio = 0.25, pos_iou_treshold = 0.5, neg_iou_treshold_hi=0.5, neg_iou_treshold_lo=0):

    ious = calc_ious(roi, bbox)

    gt_assignment = ious.argmax(axis=1)
    max_iou = ious.max(axis=1)
    gt_roi_label = labels[gt_assignment]

    pos_roi_per_image = sample * pos_ratio

    pos_index = np.where(max_iou >= pos_iou_treshold)[0]
    pos_roi_per_this_image = int(min(pos_roi_per_image, pos_index.size))
    if pos_index.size > 0:
        pos_index = np.random.choice(
            pos_index, size=pos_roi_per_this_image, replace=False)

    neg_index = np.where((max_iou < neg_iou_treshold_hi) &
                         (max_iou >= neg_iou_treshold_lo))[0]
    neg_roi_per_this_image = sample - pos_roi_per_this_image
    neg_roi_per_this_image = int(min(neg_roi_per_this_image,
                                     neg_index.size))
    if neg_index.size > 0:
        neg_index = np.random.choice(
            neg_index, size=neg_roi_per_this_image, replace=False)

    keep_index = np.append(pos_index, neg_index)
    gt_roi_labels = gt_roi_label[keep_index]
    gt_roi_labels[pos_roi_per_this_image:] = 0  # negative labels --> 0
    sample_roi = roi[keep_index]

    bbox_for_sampled_roi = bbox[gt_assignment[keep_index]]
    # print(bbox_for_sampled_roi.shape)

    height = sample_roi[:, 2] - sample_roi[:, 0]
    width = sample_roi[:, 3] - sample_roi[:, 1]
    ctr_y = sample_roi[:, 0] + 0.5 * height
    ctr_x = sample_roi[:, 1] + 0.5 * width

    base_height = bbox_for_sampled_roi[:, 2] - bbox_for_sampled_roi[:, 0]
    base_width = bbox_for_sampled_roi[:, 3] - bbox_for_sampled_roi[:, 1]
    base_ctr_y = bbox_for_sampled_roi[:, 0] + 0.5 * base_height
    base_ctr_x = bbox_for_sampled_roi[:, 1] + 0.5 * base_width

    eps = np.finfo(height.dtype).eps
    height = np.maximum(height, eps)
    width = np.maximum(width, eps)

    dy = (base_ctr_y - ctr_y) / height
    dx = (base_ctr_x - ctr_x) / width
    dh = np.log(base_height / height)
    dw = np.log(base_width / width)

    gt_roi_locs = np.vstack((dy, dx, dh, dw)).transpose()

    return gt_roi_labels, gt_roi_locs, sample_roi



