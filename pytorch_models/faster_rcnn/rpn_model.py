import torch.nn as nn
from pytorch_models.frcnn_jwyang_implement.frcnn_layers import RpnLayers


def rpn(nr_classes=2, pretrained=False):

    model = RpnLayers(nr_classes=nr_classes)

    return model

