
from pytorch_models.yolov3_implement_externalgit.train import run_main

if __name__ == "__main__":
    seed = 4

    postfix = '_2'

    target_size = 416
    learning_rate = 1e-4
    n_epochs = 40

    run_main(target_size=target_size, learning_r=learning_rate, n_epochs=n_epochs, postfix=postfix)

