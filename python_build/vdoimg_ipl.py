"""
python version of Track1/VDOIMG_IPL/
1) creates folders based on camera location S01, c001 -> Loc1_1
2) saves images from video at each camera location to /Track1/Loc1_1/img1/

!!! saved images do not match c++ version.  fewer images are saved vs the expected frame count
"""
#!/usr/bin/env python3

import os
import cv2

cwd = os.path.dirname(os.getcwd())
location = "S01"
camera = "c002"
dataPath = "D:/My documents - large/CS230_project_files/aic19-track1-mtmc/train/S03/c010/vdo.avi" #%(cwd, location, camera)
# dataPath = "%s/datasets/train/%s/%s/vdo.avi" %(cwd, location, camera)
outputPath = "D:/My documents - large/CS230_project_files/aic19-track1-mtmc/train/S03/c010/images" %(cwd)

# get location name
loc_name = "Loc%s_%s" %(int(location[1:]), int(camera[1:]))
print("location name: ", loc_name)
print("data path: ", dataPath)

# get output folder
outputPath = outputPath + "%s/img1/" %(loc_name)
print("output path: ", outputPath)

# check for output folder
if not os.path.exists(outputPath):
    os.makedirs(outputPath)
    print("making ", outputPath)

# create video reader for input video
video_capture = cv2.VideoCapture(dataPath)
video_length = int(video_capture.get(cv2.CAP_PROP_FRAME_COUNT)) - 1
print("video length: ", video_length)
frame_count = 0

# process every frame of video
while True:
    video_capture.set(cv2.CAP_PROP_POS_MSEC, (frame_count * 1000))
    success, image = video_capture.read()
    print("frame %06d" %(frame_count))
    cv2.imwrite("%s/%06d.jpg" %(outputPath, frame_count), image)

    if not success:
        print("success: ", success)
        break
    frame_count += 1


