import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split




# goal of this script is to use only limited set of the nexar images for training, as we have a small dataset of AAIC data.
# only shuffle the boxes lables (in processing a left merge occurs - that will ensure that right lables are picked)
repo_home = '/media/veracrypt1/Repository/cs230_vehicle_tracking/'
labels_nexar = repo_home + 'datasets/train/nexar/train_boxes.csv'
labels_nexet_meta = repo_home + 'datasets/train/nexar/train.csv'

label_cats = pd.read_csv(repo_home + 'datasets/train/nexar/labels.csv', names=["id", "label"], header=None)
label_cats = label_cats.set_index(["id"])
label_cats = label_cats["label"]

id_cat = label_cats.to_dict()
cat_id = {value: key for key, value in id_cat.items()}

filter_dayperiod = 'Day'

labels_boxes = pd.read_csv(labels_nexar)
# pd.read_csv(labels_nexar, names=['image_number', 'col1', 'box_x', 'box_y', 'box_width', 'box_height', 'prob', 'col2', 'col3', 'col4', 'category_id'], header=None, dtype={'image_number': str})
labels_meta = pd.read_csv(labels_nexet_meta)
labels_join = labels_boxes.merge(labels_meta, how='left', on='image_filename')

labels_join.dropna(inplace=True)

if filter_dayperiod is not 'none':
    labels_filter = labels_join[labels_join['lighting'] == filter_dayperiod]
    print(len(labels_filter))
else:
    labels_filter = labels_join.copy()
    print(len(labels_filter))

image_names = labels_filter[['image_filename']].drop_duplicates(keep='first', inplace=False)
print(len(image_names))

# use the sklearn split function (we actually won't use the test set)
train_nr, test_nr = train_test_split(image_names, test_size=0.8, random_state=0)
# train_nr['image_filename'] = train_nr['image_filename'].astype(int)
# test_nr['image_filename'] = test_nr['image_filename'].astype(int)

print(len(train_nr), len(test_nr))
# print(train_nr, test_nr)

train = pd.merge(train_nr, labels_boxes, on="image_filename", how='left')
test = pd.merge(test_nr, labels_boxes, on="image_filename", how='left')

print(len(train), len(test))

train.to_csv(repo_home + 'datasets/train/nexar/train_boxes_train.csv', index=False)
test.to_csv(repo_home + 'datasets/train/nexar/train_boxes_excluded.csv', index=False)

train_gt_formated = pd.merge(train_nr, labels_join, on="image_filename", how='left')
# test_gt_formated = pd.merge(train_nr, labels_join, on="image_filename", how='left')

train_gt_formated['id'] ='-1'
train_gt_formated['col_1'] ='-1'
train_gt_formated['col_2'] ='-1'
train_gt_formated.rename(columns={"x0": "box_x", "y1": "box_y"}, inplace=True)
train_gt_formated['box_width'] = train_gt_formated['x1'] - train_gt_formated['box_x']
train_gt_formated['box_height'] = train_gt_formated['box_y'] - train_gt_formated['y0']

train_gt_formated['label_id'] = train_gt_formated['label'].replace(cat_id)
train_gt_formated[['box_x', 'box_y', 'box_width', 'box_height']] = train_gt_formated[['box_x', 'box_y', 'box_width', 'box_height']].applymap(np.int64)

keep_cols = ['image_filename', 'id', 'box_x', 'box_y', 'box_width', 'box_height', 'confidence', 'label_id', 'col_1', 'col_2']
#['FrameId', 'Id', 'X', 'Y', 'Width', 'Height', 'Confidence', 'ClassId', 'Visibility', 'unused']

train_gt_formated = train_gt_formated[keep_cols]
train_gt_formated.sort_values('image_filename', inplace=True)
# test_gt_formated = test_gt_formated[keep_cols]

train_gt_formated.to_csv(repo_home + 'datasets/output/ssd/gt_labels/nexar_train.txt', index=False, header=False)
# test.to_csv(repo_home + 'datasets/train/nexar/train_boxes_excluded.csv', index=False)
