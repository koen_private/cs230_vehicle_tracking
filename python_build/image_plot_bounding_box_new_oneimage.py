# import boto3
import io
import pandas as pd
import glob
import os
from PIL import Image, ImageDraw, ExifTags, ImageColor, ImageFont

if __name__ == "__main__":

    label_paths = {"models": "../datasets/train/S04/c016/det/det_yolo3.txt",
                   "ssd": "../datasets/train/S04/c016/det/det_ssd512.txt",
                   "rcnn": "../datasets/train/S04/c016/det/det_mask_rcnn.txt"
                   }

    yolo = pd.read_csv(label_paths["models"], header=None, usecols=[0, 2, 3, 4, 5])
    ssd = pd.read_csv(label_paths["ssd"], header=None, usecols=[0, 2, 3, 4, 5])
    rcnn = pd.read_csv(label_paths["rcnn"], header=None, usecols=[0, 2, 3, 4, 5])

    labels = [(yolo, "models"),
              (ssd, "ssd"),
              # (rcnn, "rcnn")
              ]

    # image_path = "/home/koen/Documents/CS230/Data/Track1/Loc1_1/img1"
    image_path = "/media/veracrypt1/My documents - large/CS230_project_files/aic19-track1-mtmc/train/S04/images/c016/"
    image_save_path = "/media/veracrypt1/My documents - large/CS230_project_files/aic19-track1-mtmc/train/S04/images_boxes/c016/"
    # boolean if using original c++ images starting from 000000.jpg or git
    use_c = False
   
    if not os.path.exists(image_save_path):
        os.makedirs(image_save_path)

    for filepath in glob.glob(os.path.join(image_path, '*.jpg')):

        img_name = (filepath.split('/')[-1]).split(".")[0]
        if use_c:
            # using original c++ files (starting at 000000.jpg versus id = 1)
            img_number = int(img_name)+1
            img_name = str(img_number).zfill(6)
        else:
            img_number = int(img_name)
            img_name = str(img_number).zfill(6)
            # img_number = int(img_name)
        print("img: ", img_name)
        img_name_save = img_name + '_boxed.jpg'

        labels_temp = []
        for labelset in labels:
            labelsubset = labelset[0]
            labels_temp.append((labelsubset[labelsubset[0] == img_number],labelset[1]))
            # labels_img = labels_temp[labels_temp[0] == img_number]

        # Load image
        image = Image.open(filepath)
        # image_list = []
        # image_list.append(image)

        imgWidth, imgHeight = image.size
        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype("/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf", 28, encoding="unic")
        # font = ImageFont.truetype("arial.ttf", 20, encoding="unic")
        color_set = ['#00d400', '#d40000', '#0000d4']
        # models: green, ssd: red, rcnn: blue
        color_set_names = ['green', 'red', 'blue']
        # sets =
        i = 0

        for labelsets in labels_temp:
            labelset = labelsets[0]
            # label_path_subset =

            # print("Printing labels for %s, in color %s" % (labelsets[1], color_set_names[i]))
            n = 0
            for index, row in labelset.iterrows():

                left = row[2]
                top = row[3]
                width = row[4]
                height = row[5]
                text = str(index + 1)

                # print('Left: ' + '{0:.0f}'.format(left))
                # print('Top: ' + '{0:.0f}'.format(top))
                # print('Face Width: ' + "{0:.0f}".format(width))
                # print('Face Height: ' + "{0:.0f}".format(height))

                points = (
                    (left, top),
                    (left + width, top),
                    (left + width, top + height),
                    (left, top + height),
                    (left, top)

                )
                draw.line(points, fill=color_set[i], width=2)
                if color_set[i] == '#d40000':
                    draw.text((left, top), str(n), font=font, fill=(color_set[i]))
                # if i == 0:
                #     draw.text((left, top), text, font=font, fill=('#d4006a'))
                n += 1
            i += 1

        # Alternatively can draw rectangle. However you can't set line width.
        # draw.rectangle([left,top, left + width, top + height], outline='#00d400')

        image.save(os.path.join(image_save_path, img_name_save))
        image.close()
