import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

postfix = '_2'

repo_home = '/media/veracrypt1/Repository/cs230_vehicle_tracking/'
labels_AAIC = repo_home + 'datasets/train/S01/c001/gt/datasets_train_S01_c001_self_relabeled_int' + postfix + '.csv'


labels = pd.read_csv(labels_AAIC, names=['image_number', 'col1', 'box_x', 'box_y', 'box_width', 'box_height', 'prob', 'col2', 'col3', 'col4', 'category_id'], header=None, dtype={'image_number': str})

labels_filter = labels[labels['category_id'] != -1]
labels_filter['image_number'] = labels_filter['image_number'].astype(int)

print(len(labels_filter))

labels_filter['category_id'] = labels_filter['category_id']

image_numbers = labels_filter[['image_number']].drop_duplicates(keep='first', inplace=False)


train_nr, test_nr = train_test_split(image_numbers, test_size=0.10, random_state=0)
train_nr['image_number'] = train_nr['image_number'].astype(int)
test_nr['image_number'] = test_nr['image_number'].astype(int)

print(len(train_nr), len(test_nr))
# print(train_nr, test_nr)

train = pd.merge(train_nr, labels_filter, on="image_number", how='left')
test = pd.merge(test_nr, labels_filter, on="image_number", how='left')

print(len(train), len(test))

train.to_csv(repo_home + 'datasets/train/S01/c001/gt/datasets_train_S01_c001_self_labeled_train' + postfix + '.txt', index=False, header=False)
test.to_csv(repo_home + 'datasets/train/S01/c001/gt/datasets_train_S01_c001_self_labeled_test' + postfix + '.txt', index=False, header=False)

train_gt_formated = train.copy()
test_gt_formated = test.copy()

keep_cols = ['image_number', 'col1', 'box_x', 'box_y', 'box_width', 'box_height', 'prob', 'category_id', 'col2', 'col3']
# ['FrameId', 'Id', 'X', 'Y', 'Width', 'Height', 'Confidence', 'ClassId', 'Visibility', 'unused']

train_gt_formated = train_gt_formated[keep_cols]
test_gt_formated = test_gt_formated[keep_cols]
# train_gt_formated = train_gt_formated.round({'box_x': 0, 'box_y': 0, 'box_width': 0, 'box_height': 0, 'category_id': 0})
train_gt_formated[['box_x', 'box_y', 'box_width', 'box_height']] = train_gt_formated[['box_x', 'box_y', 'box_width', 'box_height']].applymap(np.int64)

# test_gt_formated = test_gt_formated.round({'box_x': 0, 'box_y': 0, 'box_width': 0, 'box_height': 0, 'category_id': 0})
test_gt_formated[['box_x', 'box_y', 'box_width', 'box_height']] = test_gt_formated[['box_x', 'box_y', 'box_width', 'box_height']].applymap(np.int64)

train_gt_formated.sort_values('image_number', inplace=True)
test_gt_formated.sort_values('image_number', inplace=True)

train_gt_formated.to_csv(repo_home + 'datasets/output/ssd/gt_labels/aicc_train' + postfix + '.txt', index=False, header=False)
test_gt_formated.to_csv(repo_home + 'datasets/output/ssd/gt_labels/aicc_test' + postfix + '.txt', index=False, header=False)

# label_idx x_center y_center width height
image_width = 1920
image_height = 1080
rounding = 5
keep_cols_yolo = ['image_name', 'category_id','cx','cy','box_width','box_height']
yolo_output_train = train_gt_formated
yolo_output_train['image_name'] = '00000' + yolo_output_train['image_number'].apply(str)
yolo_output_train['image_name'] = yolo_output_train['image_name'].str[-6:]
yolo_output_train['cx'] = (yolo_output_train['box_x'] + yolo_output_train['box_width']/2) / image_width
yolo_output_train['cy'] = (yolo_output_train['box_y'] + yolo_output_train['box_height']/2) / image_height
yolo_output_train['box_width'] = yolo_output_train['box_width'] / image_width
yolo_output_train['box_height'] = yolo_output_train['box_height'] / image_height
yolo_output_train['category_id'] = yolo_output_train['category_id'] - 1
yolo_output_train = yolo_output_train[keep_cols_yolo]
yolo_output_train = yolo_output_train.round({'cx': rounding, 'cy': rounding, 'box_width': rounding, 'box_height': rounding})

yolo_output_test = test_gt_formated
yolo_output_test['image_name'] = '00000' + yolo_output_test['image_number'].apply(str)
yolo_output_test['image_name'] = yolo_output_test['image_name'].str[-6:]
yolo_output_test['cx'] = (yolo_output_test['box_x'] + yolo_output_test['box_width']/2) / image_width
yolo_output_test['cy'] = (yolo_output_test['box_y'] + yolo_output_test['box_height']/2) / image_height
yolo_output_test['box_width'] = yolo_output_test['box_width'] / image_width
yolo_output_test['box_height'] = yolo_output_test['box_height'] / image_height

for index, row in yolo_output_train.iterrows():
    if row['cx'] > 1:
        print("cx issue", row['image_name'])
    if row['cy'] > 1:
        print("cy issue", row['image_name'])
    if row['box_width'] > 1:
        print("box_width issue", row['image_name'])
    if row['box_height'] > 1:
        print("box_height issue", row['image_name'])

yolo_output_test['category_id'] = yolo_output_test['category_id'] - 1
yolo_output_test = yolo_output_test[keep_cols_yolo]
yolo_output_test = yolo_output_test.round({'cx': rounding, 'cy': rounding, 'box_width': rounding, 'box_height': rounding})

grouped_train = yolo_output_train.groupby("image_name")
for name, group in grouped_train:
    write_file = name
    write_group = pd.DataFrame()
    write_group['category_id'] = group['category_id']
    write_group['cx'] = group['cx']
    write_group['cy'] = group['cy']
    write_group['box_width'] = group['box_width']
    write_group['box_height'] = group['box_height']
    write_group.to_csv(repo_home + 'datasets/train/S01/c001/gt/yolo' + postfix + '/' + write_file +'.txt', index=False, header=False, sep=' ')
yolo_output_train_files = 'data/custom/images/' + yolo_output_train['image_name'] + '.jpg'
yolo_output_train_files.drop_duplicates(inplace=True)
yolo_output_train_files.to_csv(repo_home + 'datasets/train/S01/c001/gt/yolo_self_labeled_train' + postfix + '.txt', index=False, header=False)


grouped_test = yolo_output_test.groupby("image_name")
for name, group in grouped_test:
    write_file = name
    write_group = pd.DataFrame()
    write_group['category_id'] = group['category_id']
    write_group['cx'] = group['cx']
    write_group['cy'] = group['cy']
    write_group['box_width'] = group['box_width']
    write_group['box_height'] = group['box_height']
    write_group.to_csv(repo_home + 'datasets/train/S01/c001/gt/yolo' + postfix + '/' + write_file + '.txt', index=False, header=False, sep=' ')
# yolo_output_test.to_csv(repo_home + 'datasets/train/S01/c001/gt/yolo_self_labeled_test.txt', index=False, header=False)
yolo_output_test_files = 'data/custom/images/' + yolo_output_test['image_name'] + '.jpg'
yolo_output_test_files.drop_duplicates(inplace=True)
yolo_output_test_files.to_csv(repo_home + 'datasets/train/S01/c001/gt/yolo_self_labeled_test' + postfix + '.txt', index=False, header=False)

yolo_output_train.to_csv(repo_home + 'datasets/output/yolo/gt_labels/yolo_train' + postfix + '.txt', index=False, header=False)
yolo_output_test.to_csv(repo_home + 'datasets/output/yolo/gt_labels/yolo_test' + postfix + '.txt', index=False, header=False)
# test.to_csv(repo_home + 'datasets/train/nexar/train_boxes_excluded.csv', index=Fals