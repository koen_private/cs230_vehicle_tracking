
from pytorch_models.ssd.ssd_run import run_main
import sys

if __name__ == "__main__":
    target_size = 224
    learning_rate = 1e-4
    n_epochs = 20

    postfix = '_2'

    print("target size setting: {}".format(target_size))
    run_main(mode='train', load_small=False, dataset='train', target_size=target_size, save_image=False, learning_rate=learning_rate, n_epochs=n_epochs, postfix=postfix)

    # run_main(mode='eval', load_small=False, dataset='test', target_size=target_size, save_image=False, learning_rate=learning_rate, n_epochs=n_epochs, postfix=postfix)

