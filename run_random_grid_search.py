
from pytorch_models.ssd.ssd_run import run_main
from pytorch_models.yolov3_implement_externalgit.train import run_main as yolo_main

if __name__ == "__main__":

    postfix = '_2'

    seed = 3
    random = False

    if random:
        # setting for random search
        target_grit = random.sample([224, 320, 416, 608, 800, 1024], k=3, seed=seed)
        learning_rate_grit = random.sample([5e-3, 1e-3, 5e-4, 1e-4, 1e-5, 5e-5], k=3, seed=seed)
        n_epochs_grit = random.sample([5, 10, 20, 40, 60, 100], k=3, seed=seed)
        for target_size in target_grit:
            for learning_rate in learning_rate_grit:
                for n_epochs in n_epochs_grit:
                    print("target size s, learning_rate, n_epochs setting: {}, {}, {}".format(target_size, learning_rate, n_epochs))
                    run_main(mode='train', load_small=False, dataset='train', target_size=target_size, save_image=False, learning_rate=learning_rate, n_epochs=n_epochs, postfix=postfix)

                    run_main(mode='eval', load_small=False, dataset='test', target_size=target_size, save_image=False, learning_rate=learning_rate, n_epochs=n_epochs, postfix=postfix)

                    yolo_main(target_size=target_size, learning_r=learning_rate, n_epochs=n_epochs, postfix=postfix)

    else:
        # setting for detailed search
        target_grit = [320, 416, 608]
        learning_rate_grit = [1e-3, 1e-4, 1e-5]
        n_epochs_grit = [20, 40, 60]
        for target_size in target_grit:
            for learning_rate in learning_rate_grit:
                for n_epochs in n_epochs_grit:
                    print("target size s, learning_rate, n_epochs setting: {}, {}, {}".format(target_size, learning_rate, n_epochs))
                    run_main(mode='train', load_small=False, dataset='train', target_size=target_size, save_image=False, learning_rate=learning_rate, n_epochs=n_epochs, postfix=postfix)


                    run_main(mode='eval', load_small=False, dataset='test', target_size=target_size, save_image=False, learning_rate=learning_rate, n_epochs=n_epochs, postfix=postfix)

                    yolo_main(target_size=416, learning_r=learning_rate, n_epochs=40, postfix=postfix)


