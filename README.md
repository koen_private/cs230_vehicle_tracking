# vehicle_tracking

# This GIT has been re-submitted to make it publically available

Python_builds contain tools converted to python
Python models contain the actual models in Pytorch
Two models which are working are (1) the SSD model and (2) the YoloV3 model.

The main folder contains three important run scripts:
run_ssd: run the SSD one-off including evaluation. Generates logs in the root folder.
-- before first run, be sure to adjust the paths in ssd_model_setup and run the shuffle script in pytorch_build

run_yolo: run the YoloV3. First, the shuffle script in pytorch_build should be ran. 

run_random_grid_search: supports a grid search (random or structured). 

Credits to https://github.com/parlstrand/ml_playground/blob/master/computer_vision/object_detection/ssd.ipynb and Erik Lindernoren, https://github.com/eriklindernoren/PyTorch-YOLOv3.